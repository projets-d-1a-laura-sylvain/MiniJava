open Ast
open Typed_ast
open Alloc_ast
open Utils
open X86_64

(* Allocation des variables *)

(* Constantes string *)
let (strings: (string,string) Hashtbl.t) = Hashtbl.create 17

(* Fonction pour l'allocation des expr, sexpr, access *)
(* La distinction entre ces types disparaît dans l'ast allouée *)
(* meth_ids: Map nom_meth -> id *)
(* mem_ids: Map nom_classe -> Map nom_champ -> id *)
(* env: Map nom_variable -> id *)
let rec alloc_expr meth_ids mem_ids env expr = match expr.expr with
  | TENull -> AEConst (CInt 0)
  | TEAffect (ta, te) -> 
      let e = alloc_expr meth_ids mem_ids env te in
        AEAffect(alloc_access meth_ids mem_ids env Address ta, e)
  | TESimple tse -> alloc_sexpr meth_ids mem_ids env tse
  | TEConcat (te1, te2) ->
      let e1 =alloc_expr meth_ids mem_ids env te1
      and e2 =alloc_expr meth_ids mem_ids env te2
      in AEConcat(e1, e2)
  | TEStringOfInt te ->
      let e = (alloc_expr meth_ids mem_ids env te) 
      in AEStringOfInt (e)
  | TENot te ->
      let e = alloc_expr meth_ids mem_ids env te in 
      AENot e
  | TEUminus te ->
      let e = alloc_expr meth_ids mem_ids env te in 
      AEUminus e
  | TEOp (op, te1, te2) ->
      let e1 =  alloc_expr meth_ids mem_ids env te1
      and e2 =  alloc_expr meth_ids mem_ids env te2
      in AEOp (label_from_loc "" te1.loc,op, e1, e2)

and alloc_sexpr meth_ids mem_ids env sexpr = match sexpr.sexpr with
  | TSPrint te -> AEPrint (alloc_expr meth_ids mem_ids env te)
  | TSConst cst -> (match cst with
    | CString str ->
    let str_lbl = label_from_loc "str" sexpr.loc in
    Hashtbl.add strings str_lbl str;
    AEConst (CString str_lbl)
    | _ -> AEConst cst)
  | TSEval (_, meth, expr, params) ->
    AEEval (Smap.find meth.str meth_ids, alloc_sexpr meth_ids mem_ids env expr,
          List.map (fun p -> alloc_expr meth_ids mem_ids env p) params)
  | TSNew (nt, l) ->
    let c = nt.name.str in
    AENew (c, 8*(Smap.cardinal (Smap.find c mem_ids) + 1),
    List.map (alloc_expr meth_ids mem_ids env) l)
  | TSThis      -> AEStack (Value, 16)
  | TSExpr te   -> alloc_expr meth_ids mem_ids env te
  | TSAccess ta -> alloc_access meth_ids mem_ids env Value ta

(* Paramètre val_typ : vaut Value ou Address *)
(* Value est un 'movq', utilisé pour les lectures de variable/champ *)
(* Address est un 'leaq', utilisé pour les affectations de variable/champ *)
and alloc_access meth_ids mem_ids env val_typ acc = match acc.access with
  | TAId i -> AEStack (val_typ, Smap.find i.str env)
  | TAMem (se, i) -> AEHeap (val_typ,
      alloc_sexpr meth_ids mem_ids env se,
      Smap.find i.str (Smap.find (get_class_name se.t) mem_ids))

(* Allocation des instructions *)
(* fpcur: 8*nombre de variables à portée *)
(* fpmax: 8*max du nombre de variables à portée *)
(* acc: accumulateur des instructions pour permettre fold_left *)
(* Les instructions sont en ordre inverse à la sortie du fold_left *)
let rec alloc_instr meth_ids mem_ids (env, fpcur, fpmax, acc) instr =
  match instr.instr with
  | TINop -> env, fpcur, fpmax, acc
  | TISexpr tse -> env, fpcur, fpmax,
      AISexpr (alloc_sexpr meth_ids mem_ids env tse)::acc
  | TIAffect (ta, te) -> let e = alloc_expr meth_ids mem_ids env te in
      env, fpcur, fpmax,
      AIAffect(alloc_access meth_ids mem_ids env Address ta, e)::acc
  | TIVdecl p ->
      let fpcur2 = fpcur + 8 in
      Smap.add p.name.str (-fpcur2) env, fpcur2, max fpmax fpcur2,
      AIAffect (AEStack(Address, -fpcur2), AEConst (CInt 0))::acc
  | TIVdeclinit (p, te) -> 
      let e1 = alloc_expr meth_ids mem_ids env te in
      let fpcur2 = fpcur + 8 in
      Smap.add p.name.str (-fpcur2) env, fpcur2, max fpmax fpcur2,
      AIAffect (AEStack(Address, -fpcur2), e1)::acc
  | TIIf (te,ti) ->
      let str= (label_from_loc "" te.loc) in 
      let _, _, fpmax2, instrs = alloc_instr meth_ids mem_ids (env,fpcur,fpmax,[]) ti in
      env, fpcur, fpmax2,
      AIIf (str, alloc_expr meth_ids mem_ids env te, List.rev instrs)::acc
  | TIIfElse (te, ti1, ti2) ->
      let str = (label_from_loc "" te.loc) in 
      let _, _, fpmax1, i1 = alloc_instr meth_ids mem_ids (env,fpcur,fpmax,[]) ti1 in
      let _, _, fpmax2, i2 = alloc_instr meth_ids mem_ids (env,fpcur,fpmax,[]) ti2 in 
      env, fpcur, max fpmax1 fpmax2,
      AIIfElse (str, alloc_expr meth_ids mem_ids env te,
                List.rev i1, List.rev i2)::acc
  | TIWhile (te,ti) ->
      let str = (label_from_loc "" te.loc)
      and _, _, fpmax, instr = alloc_instr meth_ids mem_ids (env,fpcur,fpmax,[]) ti in
      env, fpcur, fpmax,
      AIWhile (str, alloc_expr meth_ids mem_ids env te, List.rev instr)::acc
  | TIReturn None -> env, fpcur, fpmax, AIReturn None::acc
  | TIReturn (Some e) -> env, fpcur, fpmax,
      AIReturn (Some (alloc_expr meth_ids mem_ids env e))::acc
  | TIBlock l ->
      let _, _, fpmax, instrs =
        List.fold_left (alloc_instr meth_ids mem_ids) (env, fpcur, fpmax, acc) l
      in
      env, fpcur, fpmax, instrs

(* Ajoute les méthodes de c dans meth_names, meth_ids *)
let add_meth_id _ (c: typed_clas) (l,m) =
  Smap.fold (fun name -> fun _ -> fun (l,m) ->
    if not (Smap.mem name m) then
      name::l, Smap.add name (Meth_id.fresh()) m
    else
      l,m
  ) c.meths (l, m)

(* Numérote (fois 8) les champs d'une classe *)
(* Explore les ancêtres pour placer leurs champs en préfixe *)
(* Au total, O(n^2). Un tri topologique permettrait O(n). *)
let rec alloc_class_mems (p: typed_program) (c: typed_clas) =
  (match c.extends with
  | None -> Smap.empty
  | Some s when s.name.str = "Object" -> Smap.empty
  | Some s ->
    alloc_class_mems p (Smap.find s.name.str p.classes))
  |>
  Smap.fold (fun name -> fun _ -> fun mems ->
    Smap.add name (8*(1+Smap.cardinal mems)) mems
  ) c.mems

(* Allocation des méthodes *)
let alloc_meth meth_ids mem_ids meth =
    let env, _ = List.fold_left (fun (env, cnt) (x: param) ->
      let cnt = cnt-8 in 
      ((Smap.add x.name.str cnt env), cnt))
      (Smap.empty, 8*(List.length meth.proto.params + 3)) meth.proto.params
    in let _, _, fpmax, instrl =
      List.fold_left (alloc_instr meth_ids mem_ids)
        (env, 0, 0, []) meth.instrs
    in (fpmax, List.rev instrl)

let alloc_class_meths meth_ids mem_ids (c: typed_clas) =
  Smap.fold (fun name -> fun meth -> fun alloc_funs ->
    Mmap.add (Smap.find name meth_ids)
      (alloc_meth meth_ids mem_ids meth) alloc_funs
  ) c.meths Mmap.empty

let alloc_constr meth_ids mem_ids (c: typed_clas) =
  (* Instructions pour initialiser les champs, appeler le constructeur hérité *)
  let mems = Smap.find c.name.str mem_ids
  and extends = match c.extends with
    | None -> "Object"
    | Some t -> t.name.str
  in
  let init_vars =
    let rec aux n =
      if n = 0 then
        if c.name.str = "Object" then
          []
        else
          [AISuper extends]
      else AIAffect (
              AEHeap (Address, AEStack (Value, 16), n),
              AEConst (CInt 0))::(aux (n-8))
    in aux (8*Smap.cardinal mems)
  in
  (* Instructions du constructeur lui-même *)
  match c.constr with
    | None ->
      0, init_vars
    | Some constr ->
        let env, fpcur= List.fold_left (fun (env, fpcur) (x:param) -> 
        let fpcur= fpcur-8 in 
        ((Smap.add x.name.str fpcur env), fpcur))
        (Smap.empty, 8*(List.length constr.params + 3)) constr.params
        in let _, _, fpmax, instrs=
        List.fold_left (alloc_instr meth_ids mem_ids)
          (env, 0, 0, []) constr.instrs
      in fpmax, init_vars @ (List.rev instrs)

(* Allocation du programme entier *)
let alloc (p: typed_program) =
  let meth_names, meth_ids = Smap.fold add_meth_id p.classes ([],Smap.empty)
  and mem_ids = Smap.map (alloc_class_mems p) p.classes in
  let classes = Smap.map (fun (c: typed_clas) -> {
    name = c.name;
    paramtyps = c.paramtyps;
    extends = (match c.extends with Some t -> Some t.name.str | None -> None);
    meths = alloc_class_meths meth_ids mem_ids c;
    constr = alloc_constr meth_ids mem_ids c;
  } ) p.classes in
  let _, _, frame, instrs = List.fold_left (alloc_instr meth_ids mem_ids)
    (Smap.empty, 0, 0, []) p.main.instrs in
  { main = frame, List.rev instrs; meth_names; meth_ids; classes }

(* Production de code *)

let popn n = addq (imm n) !%rsp
let pushn n = subq (imm n) !%rsp

(* Compilation des expressions *)
let rec compile_expr = function
  | AEPrint ae ->
    compile_expr ae ++
    pushq !%rax ++
    call "print_string" ++
    popn 8
  | AENot ae  ->
    compile_expr ae ++
    notq !%rax ++
    addq (imm 2) !%rax
  | AEUminus ae ->
    compile_expr ae ++ 
    xorq !%rbx !%rbx ++
    subq !%rax !%rbx ++
    movq !%rbx !%rax
  | AEConst cst -> (match cst with
    | CInt x -> movq (imm x) !%rax
    | CBool b -> movq (imm (if b then 1 else 0)) !%rax
    | CString str_lbl ->
    let str = Hashtbl.find strings str_lbl in
    movq (imm 24) !%rdi ++ call "malloc" ++
    movq (ilab ".class_String") (ind rax) ++
    movq (imm (String.length str)) (ind ~ofs:8 rax) ++
    movq (ilab str_lbl)            (ind ~ofs:16 rax)
    )
  | AEConcat (ae1, ae2) ->
    compile_expr ae1 ++
    pushq !%rax ++
    compile_expr ae2 ++
    pushq !%rax ++
    call "concat_string" ++
    popn 16
  | AEStringOfInt ae ->
    compile_expr ae ++
    pushq !%rax ++
    call "string_of_int" ++
    popn 8
  | AEOp (str, op, ae1, ae2) -> (match op with
    | OEq | ONeq | OLt | OLeq | OGt | OGeq ->
      let o=match op with
      |OEq -> sete
      |ONeq -> setne
      |OLt -> setl
      |OLeq -> setle
      |OGt -> setg
      |OGeq -> setge
      |_ -> failwith "impossible"
      in
      compile_expr ae1 ++
      pushq !%rax ++
      compile_expr ae2 ++
      popq rbx ++
      movq (imm 0) !%r8 ++
      cmpq !%rax !%rbx ++
      o !%r8b ++
      movq !%r8 !%rax
    | OPlus | OMinus | OMul -> let o= (match op with 
       | OPlus  -> addq
       | OMinus -> subq
       | OMul   -> imulq
       | _   -> failwith "impossible") in 
      compile_expr ae1 ++ 
      pushq !%rax ++
      compile_expr ae2 ++
      popq rbx ++
      o !%rax !%rbx ++
      movq !%rbx !%rax
    | ODiv   -> 
      compile_expr ae1 ++
      pushq !%rax ++
      compile_expr ae2 ++
      movq !%rax !%rbx ++
      popq rax ++
      cqto ++
      idivq !%rbx
    | OMod   -> 
      compile_expr ae1 ++
      pushq !%rax ++
      compile_expr ae2 ++
      movq !%rax !%rbx ++
      popq rax ++
      cqto ++
      idivq !%rbx ++
      movq !%rdx !%rax
    | OAnd -> 
      compile_expr ae1 ++
      testq !%rax !%rax ++
      jz (".lazyand"^str)  ++
      compile_expr ae2 ++
      label (".lazyand"^str)
    | OOr -> 
      compile_expr ae1 ++
      testq !%rax !%rax ++
      jnz (".lazyor"^str) ++
      compile_expr ae2 ++
      label (".lazyor"^str)
      )
  | AEEval (meth, expr, params) ->
      compile_expr expr ++
      pushq !%rax ++
      List.fold_left (fun code1 code2 -> code1 ++ code2 ++ pushq !%rax) nop
        (List.map compile_expr params) ++
      movq (ind ~ofs:(8*List.length params) rsp) !%rax ++
      pushq !%rax ++
      movq (ind rsp) !%rax ++           (* Adresse de l'objet *)
      movq (ind rax) !%rax ++           (* Adresse du descripteur *)
      call_star (ind ~ofs:meth rax) ++  (* Adresse de la méthode *)
      popn (8*(List.length params + 2))
  | AEStack (vt, ofs) ->
      (match vt with
      | Value -> movq (ind ~ofs:ofs rbp) !%rax
      | Address -> leaq (ind ~ofs:ofs rbp) rax)
  | AEHeap (vt, expr, ofs) ->
      compile_expr expr ++
      (match vt with
      | Value -> movq (ind ~ofs:ofs rax) !%rax
      | Address -> leaq (ind ~ofs:ofs rax) rax)
  | AENew (c, s, l) ->
      List.fold_left (fun code1 code2 -> code1 ++ code2 ++ pushq !%rax) nop
        (List.map compile_expr l) ++
      movq (imm s) !%rdi ++
      call "malloc" ++
      movq (ilab (".class_"^c)) (ind rax) ++
      pushq !%rax ++
      call ("constr_"^c) ++
      popq rax ++
      popn (8*List.length l)
  | AEAffect (ea, e) ->
      compile_expr ea ++
      pushq !%rax ++
      compile_expr e ++
      popq rbx ++
      movq !%rax (ind rbx)

(* Compilation des instructions *)
(* fpmax: utilisé pour dépiler lors des return *)
(* code: accumulateur de code assembleur, pour fold_left *)
let rec compile_instr fpmax code = function
  | AIIf (str, ae, ai) ->
      List.fold_left (compile_instr fpmax) (code++
      compile_expr ae ++
      movq (imm 0) !%rdx ++
      cmpq !%rax !%rdx ++
      je ("if_"^str )
      ) ai ++
      label ("if_"^str)
  | AIWhile (str, ae, ai) -> 
      List.fold_left (compile_instr fpmax) (
      code ++
      label ("wloop"^str) ++
      compile_expr ae ++
      movq (imm 0) !%rdx ++
      cmpq !%rax !%rdx ++
      je ("wexit"^str)
      ) ai ++
      jmp ("wloop"^str) ++
      label ("wexit"^str)
  | AIIfElse (str, ae, ai1, ai2) ->
      List.fold_left (compile_instr fpmax) (
        List.fold_left (compile_instr fpmax) (
          code ++
          compile_expr ae ++
          movq (imm 0) !%rdx ++
          cmpq !%rax !%rdx ++
          je ("if2"^str)
        ) ai1 ++
        jmp ("if1"^str) ++
        label ("if2"^str)
      ) ai2 ++
      label ("if1"^str)
  | AISexpr ae        -> code ++ compile_expr ae
  | AINop             -> code
  | AIBlock l         -> List.fold_left (compile_instr fpmax) code l
  | AIReturn None     ->
      code ++
      popn fpmax ++
      popq rbp ++
      movq (imm 0) !%rax ++
      ret
  | AIReturn (Some e) ->
      code ++
      compile_expr e ++
      popn fpmax ++
      popq rbp ++
      ret
  | AIVdecl p         ->
      code ++ 
      movq (imm 0) (ind ~ofs:p rbp)
                
  | AIVdeclinit (p, e)->
      code ++ 
      compile_expr e ++
      movq !%rax (ind ~ofs:p rbp)
  | AIAffect (ea, e)  ->
      code ++
      compile_expr ea ++
      pushq !%rax ++
      compile_expr e ++
      popq rbx ++
      movq !%rax (ind rbx)
  (* Super: empile le 'this' puis appelle le constructeur parent *)
  | AISuper c         ->
      code ++
      movq (ind ~ofs:16 rbp) !%rax ++
      pushq !%rax ++
      call ("constr_"^c) ++
      popq rax

(* Compilation des méthodes *)
(* Cas particulier pour String.equals dont le code est dans Std *)
let compile_meth p cname id (fpmax,instrs) code =
  code ++
  label (Format.sprintf "meth_%s_%d" cname id) ++
  if cname <> "String" then
    pushq !%rbp ++
    movq !%rsp !%rbp ++
    pushn fpmax ++
    List.fold_left (compile_instr fpmax) nop instrs ++
    popn fpmax ++
    popq rbp ++
    ret
  else
    Std.string_equals

(* Compilation du constructeur *)
(* Identique à compile_meth, sauf label et cas particulier String.equals *)
let compile_constr p cname (fpmax,instrs) code =
  code ++
  label (Format.sprintf "constr_%s" cname) ++
  pushq !%rbp ++
  movq !%rsp !%rbp ++
  pushn fpmax ++
  List.fold_left (compile_instr fpmax) nop instrs ++
  popn fpmax ++
  popq rbp ++
  ret

let compile_class p meth_names meth_ids name (c: alloc_clas)
                                            (codefuns, codedescr) =
  (* METHODES *)
  (if name <> "String" then
    compile_constr p name c.constr codefuns
  else codefuns) |>
  Mmap.fold (compile_meth p name) c.meths,
  (* DESCRIPTEUR *)
  codedescr ++
  label (Format.sprintf ".class_%s" name) ++
  (if name = "Object" then
    dquad [0; 0]
  else
    address [(Format.sprintf ".class_%s"
    (match c.extends with Some s -> s | None -> "Object"));
    (Format.sprintf "constr_%s" name)]) ++
  List.fold_left (fun l -> fun name ->
    (match Typer.get_func p {str=name; loc=Utils.stupid_loc}
      {name=c.name; params=
      List.map (fun (p: ntyp) -> {name=p.name; params=[]}) c.paramtyps} with
      | None -> dquad [0]
      | Some (t,p) -> address [Format.sprintf "meth_%s_%d" t.name.str
        (Smap.find name meth_ids)]
    ) ++ l
  ) nop meth_names

let compile_program p ofile =
  let ap = alloc p in
  let main_frame,main_instrs = ap.main in
  let code = List.fold_left (compile_instr main_frame) nop main_instrs in
  let codefuns, codedescr =
    Smap.fold (compile_class p ap.meth_names ap.meth_ids) ap.classes (nop,nop)
  in
  let p =
    { text =
        globl "main" ++ label "main" ++
        pushq !%rbp ++
        movq !%rsp !%rbp ++
        pushn main_frame ++
        code ++
        popn main_frame ++
        popq rbp ++
        movq (imm 0) !%rax ++ (* exit *)
        ret ++
        codefuns ++
        Std.code;
      data =
        Hashtbl.fold (fun str_lbl str l -> label str_lbl ++ string str ++ l)
          strings (label ".str_print_string" ++ string "%s") ++
        codedescr
    }
  in
  let f = open_out ofile in
  let fmt = Format.formatter_of_out_channel f in
  X86_64.print_program fmt p;
  Format.fprintf fmt "@?";
  close_out f
