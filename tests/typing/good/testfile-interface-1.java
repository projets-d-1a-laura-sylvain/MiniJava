class A {}
class B extends A {}

interface I { A m(); }
interface J extends I { B m(); }
class Main { public static void main(String[] args) { } }
