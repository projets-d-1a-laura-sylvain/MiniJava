
class A { }
class B extends A { }
class C { B m() { return null; } }
class D extends C { A m() { return null; } }
class Main { public static void main(String[] args) { } }
