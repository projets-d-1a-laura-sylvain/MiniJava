(* Module utilisé pour associer à chaque nom de méthode un identifiant unique *)

type t = int

let next = ref 16
let fresh() = let a = !next in next := a+8; a
let compare = compare