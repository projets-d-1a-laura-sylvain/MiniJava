open Typed_ast
open Ast
open Error
open Utils
open Std

(* Tests inheritance cycles and adds "extends Object" if extends=None *)
type mark= InProgress | Visited
let test_cycle env =
  let rec auxclass (seen, env) s =
    if Smap.mem s seen then
      match Smap.find s seen with
      | InProgress ->
        inheritance_cycle {str=s; loc=find_loc s env}
      | Visited -> (seen, env)
    else
      let c = Smap.find s env.classes in
      List.iter (fun (t: ntyp) ->
        if not (is_intf env t.name.str) then
          doesnt_name "an interface" t.name
      ) c.implements;
      match c.extends with
        | None -> (Smap.add s Visited seen, {paramtyps=env.paramtyps;
            classes=Smap.add s {
              name = c.name; paramtyps = c.paramtyps;
              extends = Some {name=obj_ident; params=[]};
              implements = c.implements; funs = c.funs
            } env.classes;
            intfs=env.intfs; vars=env.vars})
        | Some t ->
          if t.name.str = "String" then
            final_class t.name
          else if is_class env t.name.str then
            let (seen,env) = auxclass (Smap.add s InProgress seen, env) t.name.str
            in (Smap.add s Visited seen, env)
          else
            doesnt_name "a class" t.name
  in let (_, env) = List.fold_left auxclass (Smap.empty, env)
    (List.map fst (Smap.bindings env.classes)) in
  let rec auxintf (seen, env) s =
    if Smap.mem s seen then
      match Smap.find s seen with
      | InProgress ->
        inheritance_cycle {str=s; loc=find_loc s env}
      | Visited -> (seen, env)
    else
      let c = Smap.find s env.intfs in
      List.fold_left (fun (seen,env) -> fun (t: ntyp) ->
        if not (is_intf env t.name.str) then
          doesnt_name "an interface" t.name
        else
          let (seen,env) = auxintf (Smap.add s InProgress seen, env) t.name.str
            in (Smap.add s Visited seen, env)
      ) (seen, env) c.extends
  in let (_, env) = List.fold_left auxintf (Smap.empty, env)
    (List.map fst (Smap.bindings env.intfs)) in env

(* Relation de sous-typage *)
let rec is_subtype env tau1 tau2 = match tau1, tau2 with
  | TBool,TBool | TInt, TInt | TNull, TNull | TNull, TNtyp _ | TVoid, TVoid -> true
  | TNtyp t1, TNtyp t2 ->
    if t2.name.str = "Object" then
      true
    else if t1.name.str = "Object" then
      false
    else if is_paramtype env t2.name.str then
      List.for_all (fun t -> is_subtype env tau1 (TNtyp t)) t2.params
    else (
      equal_ntyps t1 t2 ||
      if is_paramtype env t1.name.str then
        List.exists (fun t -> is_subtype env (TNtyp t) tau2)
          (Smap.find t1.name.str env.paramtyps).params
      else if is_class env t1.name.str then
        let c = Smap.find t1.name.str env.classes in
        let sigma = create_sigma t1.params c.paramtyps in
        (match c.extends with None -> false
        | Some t -> is_subtype env (TNtyp (exchange sigma t)) tau2)
        || List.exists (fun t -> is_subtype env (TNtyp (exchange sigma t)) tau2)
          c.implements
      else
        let i = Smap.find t1.name.str env.intfs in
        List.exists (fun t -> is_subtype env (TNtyp t) tau2) i.extends
    )
  | _ -> false

(* NType bien formé *)
let rec well_formed_ntyp env (t: ntyp) =
  if is_paramtype env t.name.str then (
    let len = List.length t.params in
    if len <> 0 then
      parameter_number t.name 0 len
  ) else if is_class env t.name.str then
    wf_st env t.name t.params (Smap.find t.name.str env.classes).paramtyps
  else if is_intf env t.name.str then
    wf_st env t.name t.params (Smap.find t.name.str env.intfs).paramtyps
  else doesnt_name "a type" t.name
(* Les types des l1 sont bien formés et sont sous-types respectifs des l2 *)
and wf_st env name l1 l2 =
  if List.length l1 = List.length l2 then
    let sigma = create_sigma l1 l2 in
    List.iter2 (fun t1 -> fun (t2: ntyp) ->
      well_formed_ntyp env t1;
      List.iter (fun t ->
        let t2 = exchange sigma t in
        if not (is_subtype env (TNtyp t1) (TNtyp t2)) then
          unexpected_type (TNtyp t1) (TNtyp t2) t1.name.loc
      ) t2.params
    ) l1 l2
  else
    parameter_number name (List.length l2) (List.length l1)

let well_formed env (t: typ) loc = match t with
  | TBool | TInt -> ()
  | TVoid | TNull -> raise (Semantic_error
    ("void and null are not well formed.", loc))
  | TNtyp t' -> well_formed_ntyp env t'

let paramtyps_not_in_list req (p: paramtyp list) (l: ntyp list) =
  List.iter (fun (t: paramtyp) -> List.iter (fun (t': ntyp) ->
    if t.name.str = t'.name.str
    then unexpected_type_string (TNtyp t) req t'.name.loc
  ) l) p

(* Checks that no type-params are used in 'extends' and 'implements' *)
let check_paramtyps_ext = function
  | Clas c -> paramtyps_not_in_list "interface" c.paramtyps c.implements;
        (match c.extends with None -> ()
        | Some t -> paramtyps_not_in_list "class" c.paramtyps [t])
  | Intf i -> paramtyps_not_in_list "interface" i.paramtyps i.extends

let check_paramtyps env (ptyps: ntyp list) =
  (* Check cycles within paramtyps inheritance *)
  let rec aux seen s =
    if is_paramtype env s then
      let t = Smap.find s env.paramtyps in
      if Smap.mem t.name.str seen then
        match Smap.find t.name.str seen with
        | InProgress -> inheritance_cycle t.name
        | Visited -> seen
      else Smap.add t.name.str Visited
        (List.fold_left (fun seen -> fun (t: ntyp) -> aux seen t.name.str)
        (Smap.add t.name.str InProgress seen) t.params)
    else seen
  in let _ = List.fold_left aux Smap.empty
    (List.map (fun (t: ntyp)->t.name.str) ptyps)
  in
  (* Check correctness of type-variable requirements *)
  let check_param_reqs (p: ntyp) =
    if is_paramtype env p.name.str then(
      let len = List.length p.params in
      if len <> 0 then
        parameter_number p.name 0 len
    ) else (
      if not (is_intf env p.name.str) then
        unexpected_type_string (TNtyp p) "interface" p.name.loc;
      well_formed_ntyp env p
    )
  in
  List.iter (fun (t: ntyp) ->
    match t.params with
      | [] -> ()
      | h::t when is_class env h.name.str ->
        well_formed_ntyp env h;
        List.iter check_param_reqs t
      | l -> List.iter check_param_reqs l
  ) ptyps

let type_proto env (p: proto) =
  (match p.t with TVoid -> () | _ -> well_formed env p.t p.name.loc);
  {
    paramtyps=env.paramtyps; classes=env.classes; intfs=env.intfs;
    vars = Smap.add "return" p.t
      (List.fold_left (fun vars -> fun (t: param) ->
        well_formed env t.t t.name.loc;
        if Smap.mem t.name.str vars then
          redefinition "parameter" t.name;
        Smap.add t.name.str t.t vars
      ) env.vars p.params)
  }

let type_proto_constr (env: typ_env) (c: constr) =
  {
    params=c.params; instrs=[]; name=c.name; env={
      paramtyps=env.paramtyps; classes=env.classes; intfs=env.intfs;
      vars = Smap.add "return" TVoid
        (List.fold_left (fun vars -> fun (t: param) ->
        well_formed env t.t t.name.loc;
        if Smap.mem t.name.str vars then
          redefinition "parameter" t.name;
        Smap.add t.name.str t.t vars
      ) env.vars c.params)
    }
  }

let intf_inheritance (env: typ_env) name (i: intf) tintf =
  check_paramtyps_ext (Intf i);
  (* Ajouter les paramtyps à l'env *)
  let env = {paramtyps=List.fold_left
    (fun m -> fun (t: ntyp) ->
      if Smap.mem t.name.str m then
        redefinition "type-variable" t.name
      else Smap.add t.name.str t m) env.paramtyps i.paramtyps;
    classes=env.classes; intfs=env.intfs; vars=env.vars} in
  (* Vérifier les héritages de l'intf et des paramtyps *)
  List.iter (well_formed_ntyp env) i.extends;
  check_paramtyps env i.paramtyps;
  Smap.add i.name.str {name= i.name; paramtyps= i.paramtyps;
    implements= i.extends; protos= Smap.empty;
    env= env} tintf

let class_inheritance (env: typ_env) name (c: clas) tcl =
  check_paramtyps_ext (Clas c);
  (* Ajouter les paramtyps à l'env *)
  let env = {paramtyps=List.fold_left
    (fun m -> fun (t: ntyp) ->
      if Smap.mem t.name.str m then
        redefinition "type-variable" t.name
      else Smap.add t.name.str t m) env.paramtyps c.paramtyps;
    classes=env.classes; intfs=env.intfs; vars=env.vars} in
  (* Vérifier les héritages de la classe et des paramtyps *)
  (match c.extends with None -> () | Some t ->
    well_formed_ntyp env t);
  List.iter (well_formed_ntyp env) c.implements;
  check_paramtyps env c.paramtyps;
  (* Ajout de 'this' aux variables, ajout de la classe à tcl *)
  Smap.add c.name.str {name= c.name; paramtyps= c.paramtyps;
    extends= c.extends; implements= c.implements; meths= Smap.empty;
    mems= Smap.empty; constr=None;
    env = {paramtyps=env.paramtyps; classes=env.classes; intfs=env.intfs;
      vars = Smap.add "this" (TNtyp{name=c.name;
        params=List.map (fun (t: ntyp) -> {name=t.name; params=[]})
        c.paramtyps}) env.vars}} tcl

let intf_protos (i: typed_intf) =
  {name= i.name; paramtyps= i.paramtyps;
    implements= i.implements;
    protos=List.fold_left (fun protos -> fun (p: proto) ->
      if Smap.mem p.name.str protos then
        redefinition "prototype" p.name;
      let _ = type_proto i.env p in
      Smap.add p.name.str p protos
    ) Smap.empty (Smap.find i.name.str i.env.intfs).protos;
    env= i.env
  }

let class_protos_mems (c: typed_clas) =
  List.fold_left (fun (c: typed_clas) -> function
    | DArg {t; name} ->
      if Smap.mem name.str c.mems then
        redefinition "member variable" name;
      well_formed c.env t name.loc;
      { name=c.name; paramtyps=c.paramtyps; extends=c.extends;
        implements=c.implements; constr=c.constr; meths=c.meths;
        mems= Smap.add name.str {t;name} c.mems;
        env=c.env }
    | DConstr constr ->
      if constr.name.str <> c.name.str then
        raise (Syntax_error
          ("Expected declaration within class '"^c.name.str^"'."));
      (match c.constr with
        | Some _ -> redefinition "constructor" constr.name
        | None ->
        { name=c.name; paramtyps=c.paramtyps; extends=c.extends;
          implements=c.implements; constr=Some (type_proto_constr c.env constr);
          meths=c.meths; mems=c.mems; env=c.env }
      )
    | DMeth meth ->
      if Smap.mem meth.proto.name.str c.meths then
        redefinition "methode" meth.proto.name;
      let env_meth = type_proto c.env meth.proto in
      { name=c.name; paramtyps=c.paramtyps; extends=c.extends;
        implements=c.implements; constr=c.constr;
        meths= Smap.add meth.proto.name.str {
          proto= meth.proto; instrs=[]; env= env_meth
        } c.meths;
        mems=c.mems; env=c.env }
  ) c (Smap.find c.name.str c.env.classes).funs 

let rec get_mem_type (prog: typed_program) mem (t: ntyp) =
  if t.name.str = "Object" || t.name.str = "Main" then
    cannot_find mem
  else let tc = Smap.find t.name.str prog.classes in
    let sigma = create_sigma t.params tc.paramtyps in
    exchange_typ sigma
      (if Smap.mem mem.str tc.mems then (Smap.find mem.str tc.mems).t
      else match tc.extends with
      | None -> cannot_find mem
      | Some ext -> get_mem_type prog mem ext)

let rec get_all_func_intf (prog: typed_program) func acc (t: ntyp) =
  let ti = Smap.find t.name.str prog.intfs in
  let sigma = create_sigma t.params ti.paramtyps in
  if Smap.mem func ti.protos then
    (exchange sigma t,
      exchange_typ_proto sigma (Smap.find func ti.protos))::acc
  else
    List.map (fun (t,p) -> exchange sigma t, exchange_typ_proto sigma p)
      (List.fold_left (get_all_func_intf prog func) acc ti.implements)

let rec get_func (prog: typed_program) func (t: ntyp) =
  if t.name.str = "Object" || t.name.str = "Main" then
    None
  else if is_paramtype prog.env t.name.str then
    get_func_among_list prog func
      (Smap.find t.name.str prog.env.paramtyps).params
  else if is_class prog.env t.name.str then
    let tc = Smap.find t.name.str prog.classes in
    let sigma = create_sigma t.params tc.paramtyps in
    if Smap.mem func.str tc.meths then
      Some (exchange sigma t,
        exchange_typ_proto sigma (Smap.find func.str tc.meths).proto)
    else match tc.extends with
    | None -> None
    | Some ext -> exchange_typ_option sigma (get_func prog func ext)
  else if is_intf prog.env t.name.str then
    List.fold_left (fun acc -> fun (t,proto) ->
      match acc with
      | None -> Some (t,proto)
      | Some (t', proto') ->
        if is_subtype prog.env proto.t proto'.t then
             Some (t', proto')
        else Some (t, proto)
    ) None (get_all_func_intf prog func.str [] t)
  else None
and get_func_among_list prog func = function
  | [] -> None
  | t::l ->
    match get_func prog func t with
    | None -> get_func_among_list prog func l
    | Some m -> Some m

let check_types_redef env proto_org proto_redef =
  if not (is_subtype env proto_redef.t proto_org.t) then
    unexpected_type proto_redef.t proto_org.t proto_redef.name.loc;
  let len1 = List.length proto_org.params
  and len2 = List.length proto_redef.params in
  if len1 <> len2 then
    parameter_number proto_redef.name len1 len2;
  List.iter2 (fun (p1: param) -> fun (p2: param) ->
    if not (equal_types p1.t p2.t) then
      unexpected_type p2.t p1.t p2.name.loc
  ) proto_org.params proto_redef.params

let check_redef_meth_class prog (tcl: typed_clas) _ (meth: typed_meth) =
  (match tcl.extends with None -> ()
  | Some nt ->
    match get_func prog meth.proto.name nt with | None -> ()
    | Some (_, proto_org) -> check_types_redef prog.env proto_org meth.proto);
  List.iter (fun (_,proto_org) ->
    check_types_redef prog.env proto_org meth.proto
  ) (List.fold_left (get_all_func_intf prog meth.proto.name.str) [] tcl.implements)

let rec has_meth tintfs tcl (tintf: typed_intf) =
  Smap.iter (fun name -> fun proto_org ->
    if not (Smap.mem name tcl.meths) then
      doesnt_override tcl.name name tintf.name.str;
  ) tintf.protos;
  List.iter (has_meth tintfs tcl)
    (List.map (fun (t: ntyp) -> Smap.find t.name.str tintfs) tintf.implements)

let rec get_meth_names (prog: typed_program) (acc: Sset.t) (nt: ntyp) =
  let ti = Smap.find nt.name.str prog.intfs in
  List.fold_left (get_meth_names prog)
    (Smap.fold (fun name -> fun _ -> fun acc -> Sset.add name acc) ti.protos acc)
    ti.implements

let rec check_redef_intfs (prog: typed_program) =
  Smap.iter (fun _ -> fun (ti: typed_intf) ->
    Sset.iter (fun name ->
      if Smap.mem name ti.protos then
        let proto_redef = Smap.find name ti.protos in
        List.iter (fun (_,proto_org) ->
          check_types_redef prog.env proto_org proto_redef
        ) (List.fold_left (get_all_func_intf prog name)
          [] ti.implements)
      else
        let protos = 
          get_all_func_intf prog name [] {name=ti.name;params=ti.paramtyps}
        in try
          let ntorg, proto_org = min_element
            (fun (nt1,proto1) -> fun (nt2,proto2) ->
            is_subtype prog.env proto1.t proto2.t) protos in 
          List.iter (fun ((ntredef: ntyp),proto_redef) ->
              let len1 = List.length proto_org.params
              and len2 = List.length proto_redef.params in
              if len1 <> len2 then
                parameter_number proto_redef.name len1 len2;
              List.iter2 (fun (p1: param) -> fun (p2: param) ->
                if not (equal_types p1.t p2.t) then
                  unexpected_type p2.t p1.t p2.name.loc
              ) proto_org.params proto_redef.params
          ) protos
        with Not_found ->
          incompatible_inheritance ti.name name
    ) (get_meth_names prog Sset.empty {name=ti.name;params=ti.paramtyps})
  ) prog.intfs

let rec check_redef_classes prog =
  Smap.iter (fun _ -> fun tcl ->
    Smap.iter (check_redef_meth_class prog tcl) tcl.meths;
    List.iter (has_meth prog.intfs tcl) 
      (List.map (fun (t: ntyp) -> Smap.find t.name.str prog.intfs)
      tcl.implements)
  ) prog.classes

let check_redef prog =
  check_redef_classes prog;
  check_redef_intfs prog

let rec type_access_var prog = function
  | AId i ->
    if is_var prog.env i.str then
      {access = TAId i; t = Smap.find i.str prog.env.vars}
    else
      (match Smap.find "this" prog.env.vars with
      | TNtyp this ->
        if is_class prog.env this.name.str then
          {access = TAMem({sexpr=TSThis; t=TNtyp this; loc=i.loc},i);
          t = get_mem_type prog i this}
        else cannot_find i
      | _ -> failwith "Incorrect type for 'this'.")
  | AMem(se,i) ->
    let tse : typed_sexpr = type_sexpr prog se in
    match tse.t with
      | TNtyp nt ->
        if is_class prog.env nt.name.str then
          {access = TAMem(tse,i);
            t = get_mem_type prog i nt}
          else cannot_find i
      | t -> unexpected_type_string t "class" tse.loc

and access_func prog = function
  | AId i ->
    (match Smap.find "this" prog.env.vars with
    | TNtyp this ->
      {sexpr=TSThis; t=TNtyp this; loc=i.loc},
      get_func prog i this
    | _ -> failwith "Incorrect type for 'this'.")
  | AMem (se, i) ->
    let tse = type_sexpr prog se in
    match tse.t with
    | TNtyp t -> tse, get_func prog i t
    | t -> unexpected_type_string t "class or interface" tse.loc

and type_expr (prog: typed_program) expr = let env=prog.env in match expr.expr with
    | ENull         ->
      {expr= TENull; t=TNull; loc = expr.loc}
    | ESimple se    ->
      let texpr = type_sexpr prog se in
      {expr= (TESimple texpr); t= texpr.t; loc= expr.loc}
    | EAffect(a, e) -> 
      let taf = (type_access_var prog a) in
      let te = type_expr prog e in
      if is_subtype env te.t taf.t then 
        {expr=TEAffect (taf, te); t= taf.t; loc= expr.loc}
      else unexpected_type te.t taf.t te.loc
    | ENot e        -> 
      let te = type_expr prog e in
      if te.t=TBool then {expr= (TENot te); t=te.t; loc=expr.loc}
      else unexpected_type te.t TBool te.loc 
    | EUminus e     -> 
      let te = type_expr prog e in
      if te.t=TInt then {expr= TEUminus te; t=te.t; loc=expr.loc}
      else unexpected_type te.t TInt te.loc
    | EOp(o,e1,e2)  ->
      let e1', e2'= type_expr prog e1, type_expr prog e2
      in match o with 
      | OEq | ONeq ->
        if (is_subtype env e1'.t e2'.t)||(is_subtype env e2'.t e1'.t)
        then {expr= TEOp (o,e1',e2') ; t=TBool; loc=expr.loc}
        else incompatible_types e1'.t e2'.t expr.loc
      | OLt | OLeq | OGt | OGeq  ->
        if e1'.t <> TInt then
          unexpected_type e1'.t TInt e1'.loc;
        if e2'.t <> TInt then
          unexpected_type e2'.t TInt e2'.loc;
        {expr= TEOp (o, e1', e2') ; t=TBool; loc=expr.loc}
      | OPlus ->
        (match e1'.t, e2'.t with
        | TInt, TInt -> {expr=TEOp (o, e1', e2'); t=TInt; loc=expr.loc}
        | TNtyp s1, TNtyp s2
          when (s1.name.str = "String" && s2.name.str = "String") ->
          {expr=TEConcat (e1', e2'); t=TNtyp s1; loc=expr.loc}
        | TNtyp s, TInt when s.name.str = "String" ->
          {expr=TEConcat (e1', {expr=TEStringOfInt e2'; t=TNtyp s;
          loc=e2'.loc}); t=TNtyp s; loc=expr.loc}
        | TInt, TNtyp s when s.name.str = "String" ->
          {expr=TEConcat ({expr=TEStringOfInt e1'; t=TNtyp s; loc=e1'.loc},
          e2'); t=TNtyp s; loc=expr.loc}
        | t1, t2 -> unexpected_type_pair t1 t2 "int or String" expr.loc
        )
      | OMinus | OMul | ODiv | OMod ->
        if e1'.t <> TInt then
          unexpected_type e1'.t TInt e1'.loc;
        if e2'.t <> TInt then
          unexpected_type e2'.t TInt e2'.loc;
        {expr= TEOp (o, e1', e2') ; t=TInt; loc=expr.loc}
      | OAnd | OOr ->
        if e1'.t <> TBool then
          unexpected_type e1'.t TBool e1'.loc;
        if e2'.t <> TBool then
          unexpected_type e2'.t TBool e2'.loc;
        {expr= TEOp (o, e1', e2') ; t=TBool; loc=expr.loc}

 and type_sexpr (prog: typed_program) sexpr = let env=prog.env in 
  match sexpr.sexpr with
  | SConst cst -> let t= (match cst with 
    |CInt (n) -> TInt
    |CBool (n) -> TBool
    |CString (n) -> TNtyp {name= {str="String"; loc= stupid_loc}; params=[]})
    in {sexpr= TSConst cst ; t=t; loc=sexpr.loc}
  | SThis ->
    let t = Smap.find "this" env.vars in
    (match t with
    | TNtyp nt when nt.name.str = "Main" ->
      raise (Semantic_error ("'this' within static context of 'main'.",
        sexpr.loc))
    | _ -> {sexpr= TSThis ; t; loc=sexpr.loc}
    )
  | SExpr e -> let te= (type_expr prog e) in
    {sexpr= TSExpr te; t= te.t; loc=sexpr.loc}
  | SNew (nt, el) ->
    well_formed_ntyp prog.env nt;
    if Smap.mem nt.name.str prog.classes then (
      let tel= List.map (fun e -> type_expr prog e) el in
      let c = Smap.find nt.name.str prog.classes in
      (match c.constr with 
        | None ->
          let len = List.length el in
          if len <> 0 then
            parameter_number nt.name 0 len;
          { sexpr = TSNew (nt,tel); t=TNtyp nt; loc=sexpr.loc}
        | Some tcstr ->
          let len1 = List.length tcstr.params
          and len2 = List.length el in
          if len1 <> len2 then
            parameter_number nt.name len1 len2;
          let sigma = create_sigma nt.params c.paramtyps in
          List.iter2 (fun (pcstr: param) -> fun (te: typed_expr) ->
            if not (is_subtype prog.env te.t (exchange_typ sigma pcstr.t)) then
              unexpected_type te.t pcstr.t te.loc)
          tcstr.params tel;
          { sexpr = TSNew (nt,tel) ; t=TNtyp nt; loc=sexpr.loc })
    ) else doesnt_name "a class" nt.name
  | SEval (acc,params) ->
    if is_system_out_print acc then (
      let len = List.length params in
      if len <> 1 then
        parameter_number {str="print"; loc=sexpr.loc} 1 len;
      let texpr = type_expr prog (List.hd params) in
      if equal_types texpr.t str_typ then
        {sexpr=TSPrint texpr; t=str_typ; loc=sexpr.loc}
      else
        unexpected_type texpr.t str_typ texpr.loc
    ) else (
      match access_func prog acc with
      | _,None -> cannot_find (access_name acc)
      | tse,Some (nt, proto) ->
        let len1 = List.length proto.params
        and len2 = List.length params in
        if len1 <> len2 then
          parameter_number {str=proto.name.str; loc=sexpr.loc} len1 len2;
        let tparams = List.map (type_expr prog) params in
        List.iter2 (fun (p1: param) -> fun (p2: typed_expr) ->
          if not (is_subtype prog.env p2.t p1.t) then
            unexpected_type p2.t p1.t p2.loc
        ) proto.params tparams;
        { sexpr = TSEval (nt, proto.name, tse, tparams); t=proto.t; loc=sexpr.loc }
    )
  | SAccess a -> let ta = type_access_var prog a in
    { sexpr = TSAccess ta; t=ta.t; loc=sexpr.loc }

let rec type_instr prog = let env = prog.env in function
  | INop -> {instr=TINop; env=env}
  | ISexpr se -> {instr=TISexpr (type_sexpr prog se); env=env}
  | IAffect (access,expr) ->
    let taccess = type_access_var prog access in
    let texpr = type_expr prog expr in
    if is_subtype env texpr.t taccess.t then
      {instr=TIAffect (taccess, texpr); env=env}
    else
      let name = (match access with AId i | AMem (_,i) -> i) in
      unexpected_type texpr.t taccess.t name.loc
  | IVdecl p ->
    if Smap.mem p.name.str env.vars then
      redefinition "variable" p.name;
    {instr=TIVdecl p; env={
      paramtyps=env.paramtyps; classes=env.classes; intfs=env.intfs;
      vars=Smap.add p.name.str p.t env.vars
    }}
  | IVdeclinit (p,expr) ->
    if Smap.mem p.name.str env.vars then
      redefinition "variable" p.name;
    let texpr = type_expr prog expr in
    if is_subtype env texpr.t p.t then
      {instr=TIVdeclinit (p,texpr); env={
        paramtyps=env.paramtyps; classes=env.classes; intfs=env.intfs;
        vars=Smap.add p.name.str p.t env.vars
      }}
    else
      unexpected_type texpr.t p.t p.name.loc
  | IIf (e,i) ->
    let te = type_expr prog e in
    if te.t <> TBool then
      unexpected_type te.t TBool e.loc;
    {instr=TIIf(te, type_instr prog i); env}
  | IIfElse (e,i1,i2) ->
    let te = type_expr prog e in
    if te.t <> TBool then
      unexpected_type te.t TBool e.loc;
    {instr=TIIfElse(te, type_instr prog i1, type_instr prog i2); env}
  | IWhile (e,i) ->
    let te = type_expr prog e in
    if te.t <> TBool then
      unexpected_type te.t TBool e.loc;
    {instr=TIWhile(te, type_instr prog i); env}
  | IBlock l ->
    let _,tl = List.fold_left (fun (env,tl) -> fun i ->
      let ti = type_instr env i in
      ({classes=prog.classes; intfs=prog.intfs; env=ti.env; main=prog.main},
        ti::tl)
    ) (prog,[]) l in
    {instr=TIBlock(List.rev tl); env}
  | IReturn (r,loc) -> let ret_typ = Smap.find "return" env.vars in
  (match r with
    | None ->
      if ret_typ = TVoid then {instr=TIReturn None; env}
      else expect_return_val ret_typ loc;
    | Some e ->
      let te = type_expr prog e in
      if is_subtype env te.t ret_typ then
        {instr = TIReturn (Some te); env}
      else
        unexpected_type te.t ret_typ e.loc;
  )

let rec always_returns i = match i.instr with
  | TIIfElse (_,i1,i2) -> always_returns i1 && always_returns i2
  | TIBlock l -> List.fold_left (fun b -> fun i -> b || always_returns i) false l
  | TIReturn _ -> true
  | _ -> false

let always_returns_list env name x =
  if not (always_returns {instr=TIBlock x; env}) then
    doesnt_always_return name

let type_class (prog: typed_program) (tc: typed_clas) =
  {
    name=tc.name; paramtyps=tc.paramtyps; extends=tc.extends;
    implements=tc.implements; mems=tc.mems; env=tc.env;
    meths=Smap.map (fun (m: typed_meth) ->
      {proto=m.proto; env=m.env;
      instrs= let instrs =
        let _,tl = List.fold_left (fun (env,tl) -> fun i ->
          let ti = type_instr env i in
          ({classes=prog.classes; intfs=prog.intfs; env=ti.env; main=prog.main},
            ti::tl)
        ) ({classes=prog.classes; intfs=prog.intfs; env=m.env; main=prog.main},[])
          (find_meth m.proto.name.str
          (Smap.find tc.name.str prog.env.classes).funs).instrs in
        List.rev tl in
        if m.proto.t <> TVoid then
          always_returns_list prog.env m.proto.name instrs;
        instrs
      }
    ) tc.meths;
    constr=match tc.constr with None -> None
      | Some constr -> Some
      {
        params=constr.params; env=constr.env; name=constr.name;
        instrs=
          let _,tl = List.fold_left (fun (env,tl) -> fun i ->
            let ti = type_instr env i in
            ({classes=prog.classes; intfs=prog.intfs; env=ti.env; main=prog.main},
              ti::tl)
          ) ({classes=prog.classes; intfs=prog.intfs; env=constr.env;
            main=prog.main},[])
            (find_constr (Smap.find tc.name.str prog.env.classes).funs).instrs in
          List.rev tl
      }
  }

let type_main (prog: typed_program) instrs =
  let env = {classes=prog.env.classes; intfs=prog.env.intfs;
    paramtyps=prog.env.paramtyps; vars = Smap.add "return" TVoid
      (Smap.add "this" (TNtyp {name={str="Main"; loc=stupid_loc};params=[]})
      prog.env.vars)
    } in
  let prog = {classes=prog.classes;intfs=prog.intfs;main=prog.main;env}
  in {
    env;
    instrs =
      let _,tl = List.fold_left (fun (env,tl) -> fun i ->
        let ti = type_instr env i in
        ({classes=prog.classes; intfs=prog.intfs; env=ti.env; main=prog.main},
          ti::tl)
      ) (prog,[]) instrs in
      List.rev tl
  }

let typer (prog: program) =
  (* Ajout des classes et interfaces dans l'environnement *)
  let env = List.fold_left (fun env -> fun clintf -> match clintf with 
  | Clas c ->
          if not (c.name.str="Main"
              || (is_class env c.name.str)
              || (is_intf env c.name.str)) then   
      {paramtyps=env.paramtyps; classes=Smap.add c.name.str c env.classes;
            intfs=env.intfs; vars=env.vars }
            else redefinition "class or interface" c.name
  | Intf i ->
          if not ((is_class env i.name.str)||(is_intf env i.name.str)) then   
      {paramtyps=env.paramtyps; classes=env.classes;
            intfs=Smap.add i.name.str i env.intfs; vars=env.vars }
            else redefinition "class or interface" i.name
    ) empty_env (prog.classes) in

  (* Vérification de l'absence de cycle  *)
  let env = test_cycle env in

  (* Vérification des héritages et paramètres de types *)
  let tclasses = Smap.fold (class_inheritance env) env.classes Smap.empty in
  let tintfs = Smap.fold (intf_inheritance env) env.intfs Smap.empty in

  (* Vérification des types des prototypes/méthodes/membres *)
  let tclasses = Smap.map class_protos_mems tclasses in
  let tintfs = Smap.map intf_protos tintfs in

  (* Vérification des redéfinitions *)
  let tprog = {classes=tclasses; intfs=tintfs; env; main={instrs=[]; env}} in
  check_redef tprog;

  (* Typage du corps des méthodes *)
  let tclasses = Smap.map (fun (tc: typed_clas) -> type_class {
    classes=tclasses; intfs=tintfs; env=tc.env;
    main={instrs=[]; env}} tc) tclasses in
  let tmain = type_main tprog prog.main.instrs in
  {classes=tclasses;intfs=tintfs;main=tmain;env}
