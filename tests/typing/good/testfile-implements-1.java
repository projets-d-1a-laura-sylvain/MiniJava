class A {}
class B extends A {}

interface I { A m(); }
class C implements I { public B m() { return null; } }
class Main { public static void main(String[] args) { } }
