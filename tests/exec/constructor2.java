class A {
  A() { System.out.print("constructeur A\n"); }
}
class B extends A {
  B(int x) { System.out.print("constructeur B, avec x="+x+"\n"); }
}
class Main {
  public static void main(String[] args) {
    new B(42);
  }
}
