
interface I { void m(); }
interface J { void n(); }
class A implements I, J { public void m() {} }
class Main { public static void main(String[] args) { } }
