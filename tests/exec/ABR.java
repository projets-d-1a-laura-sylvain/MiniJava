
/* arbres binaires de recherche */

class ABR {
    int valeur;
    ABR gauche;
    ABR droite;

    ABR(ABR g, int v, ABR d) { valeur = v; gauche = g; droite = d; }

    void insere(int x) {
	if (x == valeur) return;
	if (x < valeur) {
	    if (gauche == null)
		gauche = new ABR(null, x, null);
	    else
		gauche.insere(x);
	} else
	    if (droite == null)
		droite = new ABR(null, x, null);
	    else
		droite.insere(x);
    }

    boolean contient(int x) {
	if (x == valeur) return true;
	if (x < valeur && gauche != null) return gauche.contient(x);
	if (droite != null) return droite.contient(x);
	return false;
    }

    String toStr() {
	String s = "";
	if (gauche != null) s = gauche.toStr();
	s = s + "(" + valeur + ")";
	if (droite != null) s = s + droite.toStr();
	return s;
    }

}

class Main {

    public static void main(String[] args) {

	ABR dico = new ABR(null, 1, null);
	dico.insere(17);
	dico.insere(5);
	dico.insere(8);
        System.out.print(dico.toStr() + "\n");

	if (dico.contient(5) &&
	    ! dico.contient(0) &&
	    dico.contient(17) &&
	    ! dico.contient(3) &&
	    dico.toStr().equals("(1)(5)(8)(17)"))
	    System.out.print("ok\n");
    }

}

