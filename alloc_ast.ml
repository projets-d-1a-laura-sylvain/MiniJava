open Ast

module Mmap = Map.Make(Meth_id)

type val_typ =
  | Address
  | Value

type alloc_expr =
  | AEPrint of alloc_expr
  | AEConcat of alloc_expr * alloc_expr
  | AEStringOfInt of alloc_expr
  | AEConst of Ast.const
  | AEOp of string * op * alloc_expr * alloc_expr
  | AEEval of Meth_id.t * alloc_expr * (alloc_expr list)
  | AEAffect of alloc_expr * alloc_expr
  | AENot of alloc_expr
  | AEUminus of alloc_expr
  | AENew of string * int * alloc_expr list
      (* new: class name, size in heap, params *)
  | AEStack of val_typ * int
  | AEHeap of val_typ * alloc_expr * int

type alloc_instr =
  | AINop
  | AISexpr of alloc_expr
  | AIIf of string * alloc_expr * (alloc_instr list)
  | AIIfElse of string * alloc_expr * (alloc_instr list) * (alloc_instr list)
  | AIWhile of string * alloc_expr * (alloc_instr list)
  | AIReturn of alloc_expr option
  | AIBlock of alloc_instr list
  | AIVdecl of int
  | AIAffect of alloc_expr * alloc_expr
  | AIVdeclinit of int*alloc_expr
  | AISuper of string

type alloc_meth = int * (alloc_instr list)

type alloc_clas = {
  extends: string option; name: Ast.ident;
  paramtyps: Ast.ntyp list; meths: alloc_meth Mmap.t;
  constr: alloc_meth
}

type alloc_main = int * alloc_instr list

type alloc_programm = {
  main: alloc_main; meth_names: string list;
  meth_ids: Meth_id.t Typed_ast.Smap.t;
  classes: alloc_clas Typed_ast.Smap.t
}
