(* Ast décorée avec typage *)

open Ast

module Smap = Map.Make(String)
module Sset = Set.Make(String)

type typ_env = {paramtyps: paramtyp Smap.t; classes: clas Smap.t; intfs: intf Smap.t;
	vars: typ Smap.t}

type typed_access_desc = TAId of ident | TAMem of typed_sexpr * ident
and typed_expr_desc =
	| TENull | TESimple of typed_sexpr | TEAffect of typed_access * typed_expr
	| TENot of typed_expr | TEUminus of typed_expr
	| TEOp of op * typed_expr * typed_expr
	| TEConcat of typed_expr * typed_expr
	| TEStringOfInt of typed_expr
and typed_sexpr_desc =
	| TSConst of const | TSThis | TSExpr of typed_expr
	| TSNew of ntyp*(typed_expr list)
	| TSEval of ntyp * ident * typed_sexpr * (typed_expr list)
	| TSAccess of typed_access
	| TSPrint of typed_expr
and typed_access = {access: typed_access_desc; t: typ}
and typed_expr = {expr: typed_expr_desc;  t: typ; loc: local}
and typed_sexpr = {sexpr: typed_sexpr_desc; t: typ; loc: local}

type typed_instr_desc =
	| TINop | TISexpr of typed_sexpr | TIAffect of typed_access * typed_expr
	| TIVdecl of param | TIVdeclinit of param * typed_expr
	| TIIf of typed_expr * typed_instr | TIIfElse of typed_expr * typed_instr * typed_instr
	| TIWhile of typed_expr * typed_instr | TIBlock of typed_instr list
	| TIReturn of typed_expr option
and typed_instr = {instr: typed_instr_desc;  env: typ_env}

type typed_constr = {name: ident; params: param list; instrs: typed_instr list;
	 env: typ_env }

type typed_meth = {proto: proto; instrs: typed_instr list;  env: typ_env}

type typed_clas = { name: ident; paramtyps: paramtyp list; extends: ntyp option;
					implements: ntyp list; constr: typed_constr option;
					meths: typed_meth Smap.t;	mems: param Smap.t;
					 env: typ_env }

type typed_intf = { name: ident; paramtyps: paramtyp list;
					implements: ntyp list; protos: proto Smap.t;
					env: typ_env }

type typed_clas_intf = TClas of typed_clas | TIntf of typed_intf

type typed_clas_main = { instrs: typed_instr list;  env: typ_env }

type typed_program = { classes: typed_clas Smap.t; intfs: typed_intf Smap.t;
					main: typed_clas_main; env: typ_env  }

