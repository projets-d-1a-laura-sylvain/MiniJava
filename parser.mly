
(* Analyseur syntaxique pour mini-Java *)

%{
  open Ast
  open Error

%}

/* Déclaration des tokens */

%token EOF
%token ESP
%token COMMA
%token <int> CINT 
%token <string> CSTR
%token PLUS MINUS TIMES DIV MOD AND OR
%token <string> IDENT
%token LPAREN RPAREN
%token LACC RACC
%token LBRA RBRA
%token SEMICOL
%token STATIC
%token VOID
%token WHILE
%token IF ELSE
%token THIS
%token BOOLEAN INT
%token TRUE FALSE
%token CLASS
%token EXTENDS
%token IMPLEMENTS
%token INTERFACE
%token NEW
%token NULL
%token RETURN
%token AFFECT
%token PUBLIC
%token EQUAL NEQUAL GTHAN GEQ LTHAN LEQ
%token UMINUS
%token NOT
%token DOT

/* Priorités et associativités des tokens */
%right AFFECT
%left  OR
%left  AND
%left  EQUAL NEQUAL
%left  GTHAN GEQ LEQ LTHAN
%left  PLUS MINUS
%left  TIMES DIV MOD
%right UMINUS NOT
/* "Dangling else" */
%nonassoc RPAREN
%nonassoc ELSE

/* Point d'entrée de la grammaire */
%start prog

/* Type des valeurs renvoyées par l'analyseur syntaxique */
%type <Ast.program> prog

%%

/* Règles de grammaire */

/* Liste en sens inverse, utilisé pour éviter les conflits */
rev_list(X):
	| { [] }
	| l=rev_list(X) x=X { x::l }
;

prog:
  classintf=rev_list(class_intf)
  main=class_main EOF { {classes= List.rev classintf ; main= main} }
;

ident:
  | x=IDENT { {str=x; loc= $loc } }
;

class_intf:
  | CLASS x=ident p=paramstype  ext=extends_ntype? impl=implements
    LACC d=decl* RACC
    { Clas {name=x; paramtyps= p ; extends= ext ;
      implements=impl ; funs= d}}
  | INTERFACE x=ident p=paramstype ext=extends_ntype_list(COMMA)
    LACC protl=list(prot=proto SEMICOL { prot }) RACC
    { Intf {name=x; paramtyps=p ; extends= ext ; protos= protl}}
;

class_main:
  | CLASS cn=ident p=paramstype  ext=extends_ntype? impl=implements
  	LACC PUBLIC STATIC VOID mn=IDENT LPAREN pn=IDENT LBRA RBRA x=ident RPAREN
  		LACC i=instruction* RACC RACC
  	{
        if cn.str <> "Main" || p <> [] || ext <> None
  			|| impl <> [] || mn <> "main" || pn <> "String" then
  			raise(Syntax_error "ill-formed Main class")
        else {param=x; instrs=i}
  	}
;

extends_ntype:
  | EXTENDS n=ntype { n }
;
extends_ntype_list(X):
  | { [] }
  | EXTENDS l=separated_nonempty_list(X, ntype) { l }
;
implements:
  | { [] }
  | IMPLEMENTS l=separated_nonempty_list(COMMA, ntype) { l }
;

paramstype:
  | { [] }
  | LTHAN l=separated_nonempty_list(COMMA, paramtype) GTHAN
    { l }
;
paramtype:
  | x=ident ext=extends_ntype_list(ESP) { {name=x; params=ext}}
;

decl:
  | p=parameter SEMICOL {DArg p}
  | c=constructor { DConstr c}
  | m=methode { DMeth m}
;
constructor:
  | x=ident LPAREN p=separated_list(COMMA, parameter) RPAREN
  LACC i=instruction* RACC { {name=x; params=p; instrs=i}}
;
methode:
  | p=proto LACC i=instruction* RACC { {proto=p; instrs=i}}
;
%inline meth_type:
  | t = typechose { t }
  | VOID { TVoid}
;
%inline is_public:
  | { false }
  | PUBLIC { true }
;
proto:
  | pub=is_public mt=meth_type x=ident LPAREN pl=separated_list(COMMA, parameter) RPAREN
    { {public= pub; t=mt; name=x ; params=pl}}
;
parameter:
  | t=typechose x=ident { {t=t; name=x}}
;

typechose:
  | BOOLEAN { TBool}
  | INT { TInt}
  | nt=ntype { TNtyp nt}
;
ntype:
  | x=ident n=ntype_det
    {{name= x; params= n}}
;
ntype_det:
  | { [] }
  | LTHAN n=separated_nonempty_list(COMMA, ntype) GTHAN { n }
;

expr:
  | e = expr_sans_loc { {expr = e; loc=$loc} }
;
%inline expr_sans_loc:
  | NULL                      { ENull }
  | e=exprsimple			  { ESimple e }
  | a=access AFFECT e=expr   { EAffect (a,e) }
  | NOT e=expr                { ENot e }
  | e1=expr op=operator e2=expr {EOp (op,  e1, e2)}
  | MINUS e=expr %prec UMINUS { EUminus e}
;

access:
  | a=access_sans_loc { a}
;
%inline access_sans_loc:
  | x=ident                     { AId x}
  | e=exprsimple DOT x=ident { AMem (e,x)}
;

exprsimple:
  | e=sexpr_sans_loc { {sexpr=e; loc=$loc} }
%inline sexpr_sans_loc:
  | n= CINT { SConst( CInt n )}
  | s= CSTR { SConst (CString s )}
  | TRUE { SConst(CBool true)}
  | FALSE { SConst (CBool false )}
  | THIS { SThis }
  | LPAREN e=expr RPAREN  { SExpr e }
  | NEW n=ntype LPAREN e=separated_list(COMMA, expr) RPAREN { SNew (n,e)}
  | a=access LPAREN e=separated_list(COMMA, expr) RPAREN { SEval (a,e)}
  | a=access { SAccess a }
;
%inline operator:
  | EQUAL { OEq }
  | NEQUAL { ONeq }
  | LTHAN { OLt }
  | GTHAN { OGt }
  | LEQ { OLeq }
  | GEQ { OGeq }
  | PLUS { OPlus }
  | MINUS { OMinus }
  | TIMES  { OMul }
  | DIV { ODiv }
  | MOD { OMod }
  | AND { OAnd }
  | OR { OOr }
;

instruction:
    i=instr_sans_loc { i}
;
%inline instr_sans_loc:
  | SEMICOL { INop}
  | e=exprsimple SEMICOL { ISexpr e }
  | a=access AFFECT e=expr SEMICOL { IAffect (a,e)}
  | p=parameter SEMICOL { IVdecl p }
  | p=parameter AFFECT e=expr SEMICOL { IVdeclinit (p,e) }
  | IF LPAREN e=expr RPAREN i=instruction { IIf (e,i)}
  | IF LPAREN e=expr RPAREN i1=instruction ELSE i2=instruction { IIfElse (e,i1,i2)}
  | WHILE LPAREN e=expr RPAREN i=instruction { IWhile (e,i)}
  | LACC i=instruction* RACC { IBlock i }
  | RETURN e=expr? SEMICOL { IReturn (e,$loc) }
;
  
