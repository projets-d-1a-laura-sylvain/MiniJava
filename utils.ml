open Ast
open Typed_ast
open Lexing
open Error

(* Fausse localisation utilisée pour la librairie standard *)
let stupid_pos = {pos_fname="/var/null"; pos_lnum=42; pos_bol=57; pos_cnum=69}
let stupid_loc = (stupid_pos,stupid_pos)

(* Tests booléens pour le typer *)
let is_paramtype (env: typ_env) s = Smap.mem s env.paramtyps
let is_class (env: typ_env) s = Smap.mem s env.classes
let is_intf (env: typ_env) s = Smap.mem s env.intfs
let is_var (env: typ_env) s = Smap.mem s env.vars

(* Trouve la localisation d'une classe ou interface *)
let find_loc t env=
    if is_class env t then (Smap.find t env.classes).name.loc
    else (Smap.find t env.intfs).name.loc

(* Renvoie la liste des noms d'une liste d'interfaces,
erreur si ce ne sont pas des interfaces *)
let rec get_name_testintf intfs env (l: ntyp list) = match l with
    | [] -> []
    | {name}::q -> if is_intf env name.str then name.str::(get_name_testintf intfs env q)
                   else doesnt_name "an interface" name

(* Fonctions pour gérer les substitutions de paramètres de types *)

let create_sigma l1 l2 =
  List.fold_left2 (fun sigma ->
    fun (p1: ntyp) -> fun (p2: ntyp) ->
    Smap.add p2.name.str p1 sigma
  ) Smap.empty l1 l2

let rec exchange sigma (t: ntyp) =
  if Smap.mem t.name.str sigma then Smap.find t.name.str sigma
  else
    { name = t.name; params = List.map (exchange sigma) t.params; }

let exchange_typ sigma = function
  | TNtyp t -> TNtyp (exchange sigma t)
  | t -> t

let exchange_typ_proto sigma p =
  {public=p.public; name=p.name; t=exchange_typ sigma p.t;
  params=List.map (fun (par: param) ->
    {name=p.name; t=exchange_typ sigma par.t}) p.params}

let exchange_typ_option sigma = function
  | None -> None
  | Some (t,p) -> Some (exchange sigma t, exchange_typ_proto sigma p)

(* Égalité de types *)

let rec equal_ntyps (t1: ntyp) (t2: ntyp) =
  t1.name.str = t2.name.str && List.length t1.params = List.length t2.params
  && List.for_all2 equal_ntyps t1.params t2.params

let equal_types t1 t2 = match t1,t2 with
  | TVoid, TVoid | TNull, TNull | TInt, TInt | TBool, TBool -> true
  | TNtyp t1, TNtyp t2 -> equal_ntyps t1 t2
  | _ -> false

(* Renvoie un minimum d'une liste pour l'ordre ou semi-ordre comp,
erreur s'il n'y a pas de minimum *)
(* Note : O(n^2) ici, faisable en O(n) *)

let min_element comp l =
  match List.fold_left (fun acc -> fun a ->
    match acc with
    | Some b -> acc
    | None ->
      if List.for_all (comp a) l then Some a
      else None
  ) None l with
  | None -> raise Not_found
  | Some a -> a

(* Gestion des localisations de Lexing *)

let add_file_to_pos file lb =
  let aux p =
  {
    pos_fname=file; pos_lnum=p.pos_lnum;
    pos_bol=p.pos_bol;pos_cnum=p.pos_cnum;
  } in
  lb.lex_start_p <- aux lb.lex_start_p;
  lb.lex_curr_p <- aux lb.lex_curr_p

let label_from_loc str (begp,endp) =
  "." ^ str ^ "_" ^ string_of_int begp.pos_lnum
            ^ "_" ^ string_of_int begp.pos_cnum
            ^ "_" ^ string_of_int endp.pos_lnum
            ^ "_" ^ string_of_int endp.pos_cnum

(* Divers *)

let access_name = function
| AId i | AMem (_,i) -> i

let rec find_meth s = function
  | DMeth meth::q when meth.proto.name.str = s -> meth
  | h::q -> find_meth s q
  | [] -> raise Not_found

let rec find_constr = function
  | DConstr constr::q -> constr
  | h::q -> find_constr q
  | [] -> raise Not_found

let get_class_name = function
  | TNtyp t -> t.name.str
  | _ -> failwith "Not a ntyp"