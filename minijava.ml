(* Programme principal *)

open Format
open Lexing
open Parser
open Utils
open Compile

(*******************************)
(* Analyse des paramètres      *)

(* Message d'aide *)
let usage = "usage: minijava [options] file.java"

(* Références pouvant être initialisées dans les paramètres *)
let parse_only = ref false
let type_only = ref false
let output_file = ref ""

(* Paramètres, actions et description *)
let spec =
	[
		"--parse-only", Arg.Set parse_only, "	stop after parsing";
		"--type-only", Arg.Set type_only, "	stop after typing";
		"-o", Arg.Set_string output_file, "	set output file";
	]

(* Analyse des paramètres et nom du fichier.java à compiler *)
let file =
	let file = ref None in
	let set_file s =
		if not (Filename.check_suffix s ".java") then
			raise (Arg.Bad "No .java extension");
		file := Some s
	in
	Arg.parse spec set_file usage;
	match !file with 
		| None -> Arg.usage spec usage; exit 1
		| Some f ->
			if !output_file = "" then
				output_file := Filename.remove_extension f ^ ".s";
			f

(*******************************)
(* Compilation *)
let () =
	let c = open_in file in
	let lb = Lexing.from_channel c in
	add_file_to_pos file lb;
	try
		let p = Parser.prog Lexer.token lb in
		if not !parse_only then begin
			let p = Typer.typer p in
			if not !type_only then
				compile_program p !output_file
		end;
		close_in c
	with
		| Error.Lexing_error s ->
	Error.report (lexeme_start_p lb, lexeme_end_p lb);
	eprintf "Lexical error: %s@." s;
	exit 1
		| Parser.Error ->
	Error.report (lexeme_start_p lb, lexeme_end_p lb);
	eprintf "Syntax error.@.";
	exit 1
		| Error.Syntax_error s ->
	Error.report (lexeme_start_p lb, lexeme_end_p lb);
	eprintf "Syntax error: %s@." s;
	exit 1
		| Error.Semantic_error (s,l) ->
	Error.report l;
	eprintf "Semantic error: %s@." s;
	exit 1
		| e ->
	eprintf "Anomaly: %s\n@." (Printexc.to_string e);
	exit 2



