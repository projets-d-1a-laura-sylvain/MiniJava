all: minijava.exe
	dune exec ./minijava.exe test.java

minijava.exe: minijava.ml
	dune build minijava.exe

launch $f:
	dune exec ./minijava.exe $f

test $n: minijava.exe
	cd tests; ./test -${n} ../_build/default/minijava.exe

push:
	git push

pull:
	git pull

explain:
	menhir --base /tmp/parser --dump --explain parser.mly
	cat /tmp/parser.conflicts

clean:
	dune clean
	rm -f *.class *.s
