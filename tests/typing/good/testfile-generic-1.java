class A<T> { void m(T x) {} }
class B<T> extends A<I> { void n(T x) { m(new I()); } }
class I {}
class Main { public static void main(String[] args) { } }
