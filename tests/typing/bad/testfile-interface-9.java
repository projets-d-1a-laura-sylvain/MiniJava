
class A {}
class B extends A {}

interface I { B m(); }
interface J extends I { A m(); }
class Main { public static void main(String[] args) { } }
