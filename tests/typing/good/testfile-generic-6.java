
class A<X> { void m(X x) {} }
class B extends A<Object> {}
class C { void test() { new B().m(new C()); } }
class Main { public static void main(String[] args) { } }
