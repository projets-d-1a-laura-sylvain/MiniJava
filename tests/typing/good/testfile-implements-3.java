
interface I<X> { void m(X x); }
class A implements I<A> { public void m(A x) {} }
class Main { public static void main(String[] args) { } }
