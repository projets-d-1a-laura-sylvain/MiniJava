(* Gestion des types standard *)

open Ast
open Typed_ast
open Utils
open X86_64

(* Représentations de Object et String utilisées pour le typage *)

let obj_ident = {str="Object";loc=stupid_loc}
let obj_clas = {name=obj_ident; paramtyps=[]; extends=None; implements=[];
        funs=[]}
let str_typ = TNtyp {name={str="String"; loc=stupid_loc}; params=[]}
let str_clas = {name={str="String"; loc=stupid_loc}; paramtyps=[];
        extends=Some {name=obj_ident; params=[]};
        implements=[]; funs=[
          DMeth {proto={name={str="equals";loc=stupid_loc};
            params=[{name={str="s";loc=stupid_loc};
            t=str_typ}]; public=true; t=TBool}; instrs=[IReturn
              (Some {expr=ESimple {sexpr=SConst (CBool true);loc=stupid_loc};
              loc=stupid_loc},stupid_loc)]}
        ]}
let empty_env = {paramtyps=Smap.empty; classes=Smap.add "String" str_clas
        (Smap.add "Object" obj_clas Smap.empty); intfs = Smap.empty;
        vars = Smap.empty}

(* Cas particulier System.out.print *)

let is_system = function
  | SAccess (AId i) -> i.str = "System"
  | _ -> false
let is_system_out = function
  | SAccess (AMem (se, i)) -> i.str = "out" && is_system se.sexpr
  | _ -> false
let is_system_out_print = function
  | AId _ -> false
  | AMem (se, i) -> i.str = "print" && is_system_out se.sexpr

(* Code ASCII *)
let char_zero = 48
let char_dash = 45

(* Code des fonctions standard *)

let code =
  (*******************************)
  (* FONCTION : System.out.print *)
  (*******************************)
  label "print_string" ++
  movq (ind ~ofs:8 rsp) !%rsi ++ (* paramètre passé sur la pile*)
  movq (ind ~ofs:16 rsi) !%rsi ++ (* adresse de la chaîne *)
  (* printf("%s", str); *)
  movq (ilab  ".str_print_string") !%rdi ++
  movq (imm 0) !%rax ++
  call "printf" ++
  ret ++

  (*******************************)
  (* FONCTION : concat_string    *)
  (*******************************)
  label "concat_string" ++
  (* Allocation pour objet String *)
  movq (imm 24) !%rdi ++
  call "malloc" ++
  movq !%rax !%r14 ++
  (* Allocation pour chaîne *)
  movq (ind ~ofs:16 rsp) !%r12 ++ (* str1 *)
  movq (ind ~ofs:8 rsp) !%r13 ++  (* str2 *)
  movq (imm 1) !%rdi ++
  addq (ind ~ofs:8 r12) !%rdi ++  (* len1 *)
  addq (ind ~ofs:8 r13) !%rdi ++  (* len2 *)
  call "malloc" ++
  (* Remplissage de la chaîne *)
  (* Première chaîne*)
  movq (ind ~ofs:16 r12) !%rbx ++ (* ptr1 *)
  movq (ind ~ofs:8 r12) !%rcx ++  (* len1 *)
  movq (imm 0) !%rdx ++
  testq !%rcx !%rcx ++
  jz ".concat_string_end_while_1" ++
  label ".concat_string_while_1" ++
  movb (ind ~index:rdx ~scale:1 rbx) !%r8b ++
  movb !%r8b (ind ~index:rdx ~scale:1 rax) ++
  incq !%rdx ++
  subq (imm 1) !%rcx ++
  jnz ".concat_string_while_1" ++
  label ".concat_string_end_while_1" ++
  (* Deuxième chaîne *)
  movq !%rax !%r9 ++
  addq !%rdx !%r9 ++
  movq (ind ~ofs:16 r13) !%rbx ++ (* ptr2 *)
  movq (ind ~ofs:8 r13) !%rcx ++  (* len2 *)
  movq (imm 0) !%rdx ++
  testq !%rcx !%rcx ++
  jz ".concat_string_end_while_2" ++
  label ".concat_string_while_2" ++
  movb (ind ~index:rdx ~scale:1 rbx) !%r8b ++
  movb !%r8b (ind ~index:rdx ~scale:1 r9) ++
  incq !%rdx ++
  subq (imm 1) !%rcx ++
  jnz ".concat_string_while_2" ++
  label ".concat_string_end_while_2" ++
  (* 0-terminated strings *)
  movb (imm 0) (ind ~index:rdx ~scale:1 r9) ++
  (* len = len1+len2 *)
  addq (ind ~ofs:8 r12) !%rdx ++
  movq !%rdx (ind ~ofs:8 r14) ++
  (* Pointeur vers la chaîne *)
  movq !%rax (ind ~ofs:16 r14) ++
  (* Descripteur *)
  movq (ilab ".class_String") (ind r14) ++
  movq !%r14 !%rax ++
  ret ++

  (*******************************)
  (* FONCTION : string_of_int    *)
  (*******************************)
  (*
    Idée générale du remplissage de la chaine :

    quotient = x
    sauvegarde = rsp
    while quotient != 0:
      quotient, reste = quotient/10
      pushq reste
    len = (sauvegarde - rsp)/8
    malloc(len+1)
    i = 0
    while len != 0:
      popq reste
      rax[i] = reste
      i++
      len--
    rax[i] = 0
  *)
  label "string_of_int" ++
  pushq !%rbp ++
  movq !%rsp !%rbp ++
  (* Récupération du paramètre *)
  movq (ind ~ofs:16 rbp) !%rax ++
  xorq !%r14 !%r14 ++
  cmpq !%r14 !%rax ++
  jge ".string_of_int_while_1" ++
  subq !%rax !%r14 ++
  movq !%r14 !%rax ++
  movq (imm 1) !%r14 ++
  (* Premier while *)
  label ".string_of_int_while_1" ++
  xorq !%rdx !%rdx ++
  movq (imm 10) !%rbx ++
  idivq !%rbx ++
  pushq !%rdx ++
  testq !%rax !%rax ++
  jnz ".string_of_int_while_1" ++
  (* Calcul de la longueur et allocation de la chaîne *)
  movq !%rbp !%r12 ++
  subq !%rsp !%r12 ++
  shrq (imm 3) !%r12 ++
  addq !%r14 !%r12 ++
  label ".string_of_int_malloc" ++
  movq !%r12 !%rdi ++
  incq !%rdi ++
  call "malloc" ++
  (* Second while *)
  movq (imm 0) !%r13 ++ (* i dans le while *)
  testq !%r14 !%r14 ++
  jz ".string_of_int_while_2" ++
  movb (imm char_dash) (ind rax) ++
  incq !%r13 ++
  decq !%r12 ++
  label ".string_of_int_while_2" ++
  popq rdx ++
  addq (imm char_zero) !%rdx ++
  movb !%dl (ind ~index:r13 ~scale:1 rax) ++
  incq !%r13 ++
  subq (imm 1) !%r12 ++
  jnz ".string_of_int_while_2" ++
  movb (imm 0) (ind ~index:r13 ~scale:1 rax) ++
  movq !%rax !%r12 ++
  (* Allocation et construction de l'objet *)
  movq (imm 24) !%rdi ++
  call "malloc" ++
  movq (ilab ".class_String") (ind rax) ++
  movq !%r13 (ind ~ofs:8 rax) ++
  movq !%r12 (ind ~ofs:16 rax) ++
  popq rbp ++
  ret ++

  (*******************************)
  (* FONCTION : String()         *)
  (*******************************)
  label "constr_String" ++
  movq (imm 1) !%rdi ++
  call "malloc" ++
  movb (imm 0) (ind rax) ++
  movq !%rax !%rbx ++
  popq rax ++
  movq (imm 0) (ind ~ofs:8 rax) ++
  movq !%rbx (ind ~ofs:16 rax) ++
  ret

  (*******************************)
  (* FONCTION : String.equals    *)
  (*******************************)
  (* Contient seulement le corps de la fonction,
  puisque String.equals est compilé avec les autres
  méthodes *)
let string_equals =
  movq (ind ~ofs:8 rsp) !%rax ++
  movq (ind ~ofs:16 rsp) !%rbx ++
  movq (ind ~ofs:8 rax) !%rcx ++
  movq (ind ~ofs:8 rbx) !%rdx ++
  movq (ind ~ofs:16 rax) !%rax ++
  movq (ind ~ofs:16 rbx) !%rbx ++
  cmpq !%rcx !%rdx ++
  jnz ".string_equals_ret_0" ++
  movq (imm 0) !%rdx ++
  label ".string_equals_loop" ++
  movb (ind ~index:rdx ~scale:1 rax) !%r8b ++
  testb !%r8b !%r8b ++
  jz ".string_equals_ret_1" ++
  cmpb !%r8b (ind ~index:rdx ~scale:1 rbx) ++
  jnz ".string_equals_ret_0" ++
  incq !%rax ++
  incq !%rbx ++
  jmp ".string_equals_loop" ++
  label ".string_equals_ret_1" ++
  movq (imm 1) !%rax ++
  ret ++
  label ".string_equals_ret_0" ++
  movq (imm 0) !%rax ++
  ret