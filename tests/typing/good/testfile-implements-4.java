
interface I<X> { void m(X x); }
class A<Y> implements I<Y> { public void m(Y x) {} }

class Main { public static void main(String[] args) { } }
