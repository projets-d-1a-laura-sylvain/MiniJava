/*
  parcours infixe d'un arbre binaire, en espace constant
  et en temps linéaire

  Joseph M. Morris
  Traversing binary trees simply and cheaply
  Information Processing Letters 9(5), 1979
*/

class Bintree<T> {
  T value;
  Bintree<T> left;
  Bintree<T> right;

  Bintree(Bintree<T> left, T value, Bintree<T> right) {
    this.left = left; this.value = value; this.right = right;
  }

}

interface Printable {
  void print();
}

class Int implements Printable {
  int v;
  Int(int v) { this.v = v; }
  public void print() { System.out.print(v + "."); }
}

class Trees {
  Bintree<Int> perfect(int h) {
    if (h == 0)
      return null;
    return new Bintree<Int>(perfect(h-1), new Int(h), perfect(h-1));
  }

  Bintree<Int> left(int h) {
    Bintree<Int> t = null;
    while (h > 0) {
      t = new Bintree<Int>(t, new Int(h), null);
      h = h - 1;
    }
    return t;
  }

  Bintree<Int> right(int h) {
    Bintree<Int> t = null;
    while (h > 0) {
      t = new Bintree<Int>(null, new Int(h), t);
      h = h - 1;
    }
    return t;
  }
}

class Morris<T extends Printable> {
  void inorder(Bintree<T> t) {
    while (t != null) {
      if (t.left == null) {
        // pas de sous-arbre gauche : on visite le noeud et on descend à droite
        t.value.print(); // <-- visiter t
        t = t.right;
      } else {
        // un sous-arbre gauche : on cherche le prédécesseur p et t
        Bintree<T> p = t.left;
        while (p.right != null && p.right != t) {
          p = p.right;
        }
        if (p.right == null) {
          // on a trouvé p . on le fait pointer sur t et on descend à gauche
          p.right = t;
          t = t.left;
        } else {
          // on est retombé sur t . on avait déjà visité le sous-arbre gauche
          // on restaure p.right, on visite t et on descend à droite
          p.right = null;
          t.value.print(); // <-- visiter t
          t = t.right;
        }
      }
    }
    System.out.print("\n");
  }
}

class Main {
  public static void main(String[] args) {
    Morris<Int> m = new Morris<Int>();
    Bintree<Int> t = null;
    Trees trees = new Trees();
    m.inorder(t);
    t = new Bintree<Int>(null, new Int(42), null);
    m.inorder(t);
    t = new Bintree<Int>(null, new Int(41), t);
    m.inorder(t);
    t = new Bintree<Int>(t, new Int(43), null);
    m.inorder(t);
    t = trees.perfect(3);
    m.inorder(t);
    t = trees.perfect(5);
    m.inorder(t);
    t = trees.left(5);
    m.inorder(t);
    t = trees.right(5);
    m.inorder(t);
  }
}
