
class A implements I { public void m() {} }
interface I { void m(); }
class D<X extends A & I> {}
class Test { void test(D<A> c) {} }
class Main { public static void main(String[] args) { } }
