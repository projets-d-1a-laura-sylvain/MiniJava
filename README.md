# MiniJava

## Utilisation

Pour compiler et lancer le compilateur sur `test.java`, exécuter `make`.

Pour compiler, exécuter `make minijava.exe`. Nous utilisons dune, l'exécutable se trouve dans `_build/default/minijava.exe`.

Pour lancer le compilateur sur le fichier `chemin/vers/source.java`, exécuter `make launch f=chemin/vers/source.java`.

Pour lancer le script de tests donné sur le site du cours, exécuter `make test n=i` (avec `i=1` pour lexer/parser, `i=2` pour typer, `i=3` pour exec et `i=all` pour tous les tests).

Pour nettoyer, `make clean`

## Remarques et problèmes - lexer, parser, typer

### Redéfinition de fonctions dans les interfaces

Il s'agit de différencier les deux cas suivants :

```java
class A {}
class B {}
interface I { A f(); }
interface J { B f(); }
interface K extends I,J {}
```
```java
class A {}
class B extends A {}
interface I { A f(); }
interface J { B f(); }
interface K extends I,J {}
```
Dans le premier cas, il y a une erreur (le compilateur ne sait pas quel type de retour prendre pour `f()` dans `K`), tandis que dans le second `B` est un sous-type de `A` et le compilateur peut-donc choisir `B` comme type de retour.

La solution mise en pratique consiste à chercher un minimum des types de retour des différents prototypes de la méthode pour la relation d'ordre du sous-typage : s'il en existe un, on peut le prendre comme type de retour, sinon, il y a une erreur (il y a alors deux types minimaux qui ne sont pas sous-type l'un de l'autre).

### Précision de la localisation

Nous avons plusieurs fois changé la précision de notre système de localisation: il attribuait d'abord à chaque objet une localisation, mais la lisibilité du code en pâtissait. Ne localiser que les `ident`  était en revanche trop imprécis: localiser les expressions et les idents suffit à signaler toutes les erreurs que nous renvoyons pour le moment.

### Conflit class/class Main

En utilisant une simple `list(class_intf)`, mehnir indiquait un conflit shift-reduce entre cette liste et la classe Main. La solution que nous avons trouvée est d'inverser le sens de la liste : la dérivation est `rev_list(X) -> rev_list(X) X` plutôt que `list(X) -> X list(X)`.

### Cas particulier de la classe Main pour les champs et méthodes

En raison des restrictions du minijava par rapport à java (pas de variables statiques, pas de tableaux), un appel récursif dans le `main`, si exécuté, mènerait nécessairement à une boucle infinie. Ainsi, le typer actuel interdit purement et simplement la recherche de champs et méthodes dans la classe `Main`.

### Traitement de Object, String, System

Dans l'état actuel du typer, `Object`, `String` et `System` sont définis dans `std.ml`. 

* `Object` est une classe (vide) dont toute classe qui n'hérite de rien d'autre hérite.
* Les `String` sont gérées comme une classe héritant de `Object`. La méthode `equals` est définie avec le bon prototype mais un corps trivial `return true;`, elle sera traitée lors de la production de code.
* `System` n'est pas défini à proprement parler : les appels à `System.out.print` sont gérés comme un cas particulier d'appel à une fonction: voir le cas `SEval` du typage des expressions simples.

## Remarques et problèmes - Production de code

### Stratégie de passage des paramètres

Nous suivons les recommandations du sujet : paramètres passés sur la pile et `this` ajouté en dernier sur la pile.

Afin de respecter la sémantique d'évaluation des paramètres, le code produit calcule d'abord l'objet dont la méthode est appelée, l'empile, calcule les paramètres et les empile, puis va chercher la valeur empilée en premier pour le rempiler au-dessus (`this`).

### Représentation des objets et descripteurs

La représentation des objets suit les recommandations du sujet.

On associe à chaque nom de méthode du code un identifiant (module `Meth_id`), ce qui permet d'utiliser le format suivant pour les descripteurs (on suppose que `X extends Y`) :
```assembly
.class_X:
	dquad .class_Y, .constr_X
	dquad meth_X_16
	dquad meth_Y_24
	dquad 0
	...
```
On stocke le descripteur du parent, l'adresse du constructeur de `X`, puis pour chaque nom de méthode, l'adresse de la méthode si elle existe (cela peut-être une méthode de `X` ou de l'un de ses ancêtres). Si la méthode n'existe pas, on met simplement `0`. Les identifiants de méthodes sont des multiples de 8 commençant à 16, de sorte que pour un identifiant `id` et un descripteur de classe à l'adresse `descr` l'adresse de la méthode est `descr+id`.

### Librarie standard

Le module `Std` contient le code correspondant aux objets standard, en particulier la gestion des `String`. Les `String` sont représentées par leur longueur puis un pointeur vers la chaîne de caractères correspondante, terminée par un `0`.

### Labels

De façon à générer des labels uniques, nous avons utilisé à plusieurs reprise la localisation d'une expression : ainsi, le label correspondant à `if(expr) instr;` sera `.if_l1_c1_l2_c2` où `l1,c1,l2,c2` sont respectivement les lignes et colonnes du début et de la fin de `expr`.

Pour les méthodes, le label `meth_Classe_id` est unique par construction, de même pour le label `constr_Classe` (il existe un unique constructeur pour chaque classe).

### Constructeurs

Les constructeurs sont compilés de la façon suivante (exemple pour `A` héritant de `B`) :
```
constr_A:
	pour tout champ c,
		c <- 0
	appel constr_B
	code constructeur
```

Ceci garantit que chaque champ est initialisé à `null` et que le constructeur de la classe parent est appelé.

Le compilateur ne vérifie pas que le constructeur ainsi appelé ne prend pas de paramètre.

### Conventions d'appel

La seule convention d'appel respectée dans l'ensemble du compilateur est que la valeur de retour d'une fonction est placée dans `%rax`. Les conventions de passage des paramètre sont évidemment respectées lors des appels à `malloc` et `printf`.

Les expressions placent aussi le résultat dans `%rax`. Initialement, nous le placions sur la pile, mais cela insérait beaucoup d'instructions inutiles (souvent des `pushq !%rax` et `popq !%rax` successifs).

### Initialisation des variables

Toute variable déclarée sans initialisation est initialisée à `0` (que ce soit un pointeur, un entier ou un booléen).