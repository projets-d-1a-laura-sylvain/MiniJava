
interface I { void m(); }
interface J extends I {}
class A implements J { }
class Main { public static void main(String[] args) { } }
