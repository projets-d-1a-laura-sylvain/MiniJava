/* Une implémentation des listes circulaires doublement chaînées. */
class LinkedList<T> {

    T head;
    LinkedList<T> next;
    LinkedList<T> prev;
    int size;
    LinkedList<T> iterator;

    /* Construit une nouvelle liste vide. */
    LinkedList(T elt) {
        head = elt; next = prev = this; size = 0; iterator = null;
    }

    LinkedList<T> copy() {
      LinkedList<T> l = new LinkedList<T>(this.head);
      l.next = this.next;
      l.prev = this.prev;
      l.iterator = this.iterator;
      return l;
    }

    /* Ajoute en position [idx] (0 pour insérer en tête).
       Renvoie [true] si [elt] est bien ajoutee ;
       [false] si [idx] est plus grand que la taille de la liste. */
    boolean add(int idx, T elt) {
	if (idx < 0 || idx > size)
	    return false;
	add_aux(idx, elt);
	return true;
    }

    void add_aux(int idx, T elt) {
	if (idx == 0) {
	    addFirst(elt);
	    return;
	}
	LinkedList<T> cur = get_aux(idx + 1);
	LinkedList<T> l = new LinkedList<T>(elt);
	l.next = cur;
	l.prev = cur.prev;
	cur.prev.next = l;
	cur.prev = l;
	size = size+1;
    }

    /* Ajout en tête. */
    void addFirst(T elt) {
	if (size > 0) {
	    LinkedList<T> l = this.copy();
	    next = l;
	    l.prev = this;
	    l.next.prev = l;
	}
	head = elt;
	size = size+1;
    }

    /* Ajout en queue. */
    void addLast(T elt) {
	add_aux(size, elt);
    }

    /* Vide la liste. */
    void clear() {
	size = 0;
	head = null;
	next = null;
	prev = null;
	iterator = null;
    }

    /* Renvoie [true] si la liste contient [elt]; [false] sinon.
       (tests effectués avec l'égalité physique). */
    boolean contains(T elt) {
	LinkedList<T> cur = this;
	int i = 0;
        while (i < size) {
	    if (elt == cur.head)
		return true;
	    cur = cur.next;
            i = i + 1;
	}
	return false;
    }

    /* Renvoie le idx-ième élément s'il existe; [null] sinon. */
    T get(int idx) {
	if (idx < 1 || idx > size)
	    return null;
	return get_aux(idx).head;
    }

    /* Renvoie le premier élément si la liste est non vide.
       Plus efficace que [get(1)]. */
    T getFirst() {
	return head;
    }

    /* Renvoie le dernier élément si la liste est non vide.
       Plus efficace que [get(size)]. */
    T getLast() {
	if (prev == null)
	    return null;
	return prev.head;
    }

    LinkedList<T> get_aux(int idx) {
	LinkedList<T> cur = this;
	if (idx <= size / 2) {
            int i = 1;
	    while (i < idx) {
              cur = cur.next;
              i = i + 1;
            }
	} else {
            int i = size;
	    while (i >= idx) {
		cur = cur.prev;
                i = i - 1;
            }
        }
	return cur;
    }

    /* Renvoie la 1ère position de [elt] dans la liste ou [-1] s'il n'existe
       pas. */
    int indexOf(T elt) {
	LinkedList<T> cur = this;
        int i = 1;
	while (i <= size) {
	    if (cur.head == elt)
		return i;
	    cur = cur.next;
            i = i +1;
	}
	return -1;
    }

    /* Renvoie la dernière position de [elt] dans la liste ou [-1] s'il
       n'existe pas. */
    int lastIndexOf(T elt) {
	LinkedList<T> cur = prev;
        int i = size;
	while (i > 0) {
	    if (cur.head == elt)
		return i;
	    cur = cur.prev;
            i = i - 1;
	}
	return -1;
    }

    /* Supprime le idx-ième élément de la liste s'il existe et le renvoie.
       Renvoie [null] sinon.
       L'itérateur est déplacé vers l'élément suivant si l'élément supprimé
       est celui sur lequel il est. */
    T remove(int idx) {
	if (idx <= 0 || idx > size)
	    return null;
	return remove_aux(idx);
    }

    T remove_aux(int idx) {
	if (idx == 1) {
	    T elt = head;
	    return removeFirst();
	}
	LinkedList<T> cur = get_aux(idx);
	LinkedList<T> l = cur.next;
	cur.prev.next = cur.next;
	l.prev = cur.prev;
	size = size - 1;
	if (iterator == cur)
	    iterator = l;
	return cur.head;
    }

    /* Supprime le premier élément si la liste est non vide et le renvoie.
       Renvoie [null] sinon.
       L'itérateur est déplacé vers le 2ème élément s'il est sur le premier
       élément. */
    T removeFirst() {
	T elt = head;
	if (size == 1) {
	    size = 0;
	    head = null;
	    iterator = null;
	} else {
	    head = next.head;
	    LinkedList<T> l = next;
	    next = next.next;
	    next.prev = this;
	    size = size - 1;
	    l.clear();
	}
	return elt;
    }

    /* Supprime le dernier élément si la liste est non vide et le renvoie.
       Renvoie [null] sinon.
       L'itérateur est déplacé vers le 1er élément s'il est sur le dernier
       élément. */
    T removeLast() {
	return remove(size);
    }

    /* Modifie en place le [idx]-ième élément.
       Renvoie l'ancienne valeur (si elle existe); [null] sinon. */
    T set(int idx, T elt) {
	if (idx <= 0 || idx > size)
	    return null;
	LinkedList<T> cur = get_aux(idx);
	T h = cur.head;
	cur.head = elt;
	return h;
    }

    /* Renvoie le nombre d'éléments. */
    int size() {
	return size;
    }

    /* Déplace l'itérateur vers l'élément suivant et renvoie ce dernier.
       Si l'itérateur est en fin de liste, déplace l'itérateur vers le premier
       élément.
       Renvoie [null] si la liste est vide. */
    T next() {
	if (iterator == null) {
	    if (size == 0)
		return null;
	    iterator = this;
	} else
	    iterator = iterator.next;
	return iterator.head;
    }

    /* Déplace l'itérateur vers l'élément précédent et renvoie ce dernier.
       Si l'itérateur est en début de liste, déplace l'itérateur vers le
       dernier élément.
       Renvoie [null] si la liste est vide. */
    T prev() {
	if (iterator == null) {
	    if (size == 0)
		return null;
	    iterator = this.prev;
	} else
	    iterator = iterator.prev;
	return iterator.head;
    }

}

// Classes de tests

class Int {
    int n;
    Int(int n) { this.n = n; }
}

class Main {
    public static void main(String[] args) {
	LinkedList<Int> l = new LinkedList<Int>(null);
        int i = 0;
	while (i < 5) {
	    l.addFirst(new Int(i));
            i = i + 1;
        }
	boolean b = true;
  	Int quinze = new Int(15);
	b = b && l.add(4, quinze);
	b = b && l.add(2, quinze);
	b = b && l.add(6, new Int(11));
	b = b && ! l.add(9, new Int(2000));
  	l.addLast(new Int(8));
  	l.add(0, new Int(0));
	b = b && l.size() == 10;
	b = b && l.contains(quinze) && ! l.contains(new Int(15));
	b = b && (l.get(7)).n == 15 && (l.get(3)).n == 3;
	b = b && l.get(11) == null;
	b = b && (l.getFirst()).n == 0;
	b = b && (l.getLast()).n == 8;
	b = b && l.indexOf(quinze) == 4 && l.lastIndexOf(quinze) == 7;
	b = b && (l.remove(4)).n == 15 && (l.remove(1)).n == 0;
	b = b && (l.remove(8)).n == 8;
	b = b && l.remove(8) == null;
	b = b && (l.removeFirst()).n == 4 && (l.removeLast()).n == 0;
	b = b && (l.set(2, quinze)).n == 2 && (l.get(2)).n == 15;
	b = b && (l.set(4, new Int(7))).n == 15 && (l.get(4)).n == 7;
	b = b && l.set(6, quinze) == null;
    	b = b && (l.next()).n == 3;
    	b = b && (l.next()).n == 15;
    	b = b && (l.next()).n == 1;
    	b = b && (l.next()).n == 7;
    	b = b && (l.next()).n == 11;
    	b = b && (l.next()).n == 3;
    	b = b && (l.prev()).n == 11;
    	b = b && (l.prev()).n == 7;
    	b = b && (l.prev()).n == 1;
    	b = b && (l.prev()).n == 15;
    	b = b && (l.prev()).n == 3;
    	b = b && (l.prev()).n == 11;

	if (b)
	    System.out.print("ok\n");
    }
}
