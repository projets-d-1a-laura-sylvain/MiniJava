
/* Random access lists (Okasaki)
 *
 * un exemple de récursion polymorphe
 */

interface RandomAccessList<E> {
  int length();

  RandomAccessList<E> cons(E x);

  E get(int i);

}

class Empty<E> implements RandomAccessList<E> {
  Empty() {
  }

  public int length() {
    return 0;
  }

  public E get(int i) {
    // throw new NoSuchElementException();
    return null;
  }

  public RandomAccessList<E> cons(E x) {
    return new One<E>(x, new Empty<Pair<E, E>>());
  }
}

class Zero<E> implements RandomAccessList<E> {
  RandomAccessList<Pair<E, E>> s;

  Zero(RandomAccessList<Pair<E, E>> s) {
    this.s = s;
  }

  public int length() {
    return 2 * s.length();
  }

  public E get(int i) {
    Pair<E, E> x = this.s.get(i / 2);
    if (i % 2 == 0)
      return x.fst;
    else
      return x.snd;
  }

  public RandomAccessList<E> cons(E x) {
    return new One<E>(x, this.s);
  }

}

class One<E> implements RandomAccessList<E> {
  E e;
  RandomAccessList<Pair<E, E>> s;

  One(E e, RandomAccessList<Pair<E, E>> s) {
    this.e = e;
    this.s = s;
  }

  public int length() {
    return 1 + 2 * s.length();
  }

  public E get(int i) {
    if (i == 0)
      return this.e;
    return (new Zero<E>(this.s)).get(i - 1);
  }

  public RandomAccessList<E> cons(E x) {
    return new Zero<E>(this.s.cons(new Pair<E,E>(x, this.e)));
  }

}

class Pair<A, B> {
  A fst;
  B snd;

  Pair(A a, B b) {
    this.fst = a;
    this.snd = b;
  }
}

class Int {
  int value;
  Int(int value) { this.value = value; }
}

class Main {
  public static void main(String[] args) {
    RandomAccessList<Int> s = new Empty<Int>();
    int i = 1;
    while (i <= 10) {
      s = s.cons(new Int(i));
      i = i + 1;
    }
    i = 0;
    while (i < 10) {
      System.out.print(s.get(i).value + " ");
      i = i + 1;
    }
    System.out.print("\n");
  }
}
