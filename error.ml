open Ast
open Lexing

exception Lexing_error of string
exception Syntax_error of string
exception Semantic_error of string*local

(* Pretty-printers *)
let rec print_ntyp fmt (t: ntyp) =
  Format.fprintf fmt "%s%a" t.name.str print_param_list t.params
and print_param_list fmt = function
  | [] -> ()
  | l -> Format.fprintf fmt "<@[%a@]>" print_ntyp_list l
and print_ntyp_list fmt = function
  | [] -> ()
  | h::[] -> Format.fprintf fmt "%a" print_ntyp h
  | h::t -> Format.fprintf fmt "%a,@ %a" print_ntyp h print_ntyp_list t

let print_typ fmt = function
  | TVoid -> Format.fprintf fmt "void"
  | TInt -> Format.fprintf fmt "int"
  | TNull -> Format.fprintf fmt "nulltype"
  | TBool -> Format.fprintf fmt "bool"
  | TNtyp t -> print_ntyp fmt t

(* Report localisation *)
let report (b,e) =
  let l = b.pos_lnum in
  let fc = b.pos_cnum - b.pos_bol + 1 in
  let lc = e.pos_cnum - b.pos_bol + 1 in
  Format.eprintf "File \"%s\", line %d, characters %d-%d:\n" b.pos_fname l fc lc

(* ERRORS *)

(* Generic *)

let redefinition qual id =
    raise (Semantic_error ("Redefinition of "^qual^" '"^id.str^"'.", id.loc))

let doesnt_name qual id =
    raise (Semantic_error ("'"^id.str^"' doesn't name "^qual^".", id.loc))

let cannot_find (name: ident) =
    report name.loc;
    Format.eprintf "Error: cannot find symbol '%s'.@." name.str;
    exit 1

let debug str =
    Format.eprintf "Debug: %s@." str

(* Type errors *)

let incompatible_types t1 t2 loc =
    report loc;
    Format.eprintf "Error: Incompatible types: '%a', '%a'.@."
        print_typ t1 print_typ t2;
    exit 1

let unexpected_type t1 t2 loc =
    report loc;
    Format.eprintf "Error: Unexpected type: '%a', expected '%a'.@."
        print_typ t1 print_typ t2;
    exit 1

let unexpected_type_string t1 str loc =
    report loc;
    Format.eprintf "Error: Unexpected type: '%a', expected %s.@."
        print_typ t1 str;
    exit 1

let unexpected_type_pair t1 t2 s loc =
    report loc;
    Format.eprintf "Error: Unexpected types: '%a', '%a'. Expected %s"
        print_typ t1 print_typ t2 s;
    exit 1

(* Inheritance-related *)

let inheritance_cycle (name: ident) =
    report name.loc;
    Format.eprintf "Error: Inheritance cycle involving '%s'.@." name.str;
    exit 1

let final_class (name: ident) =
    report name.loc;
    Format.eprintf "Error: '%s' is a final class.@." name.str;
    exit 1

let doesnt_override (c: ident) m i =
    report c.loc;
    Format.eprintf "Error: '%s' doesn't override method '%s' in '%s'.@." c.str m i;
    exit 1

let incompatible_inheritance (i: ident) meth =
    report i.loc;
    Format.eprintf "Error: Incompatible inheritances in '%s':@ \
multiple incoherent definitions of '%s'.@."
        i.str meth;
    exit 1

(* Method-related *)

let parameter_number (name: ident) n1 n2 =
    report name.loc;
    Format.eprintf "Error: '%s' expects %u parameters instead of %u.@."
        name.str n1 n2;
    exit 1

let expect_return_val t loc =
    report loc;
    Format.eprintf "Error: expected return value of type '%a'.@." print_typ t;
    exit 1

let doesnt_always_return (i: ident) =
    report i.loc;
    Format.eprintf "Error: Function '%s' doesn't always return.@." i.str;
    exit 1
