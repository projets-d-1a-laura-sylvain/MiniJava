(* Module utilisé pour associer à chaque nom de méthode un identifiant unique *)

type t = int

val fresh: unit -> t
val compare: t -> t -> int