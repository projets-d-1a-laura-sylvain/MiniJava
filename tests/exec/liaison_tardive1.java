class A {
    A() {}
    boolean f() { return true; }
}

class B extends A {
    B() {}
    boolean f() { return false; }
}

class Main {
    public static void main(String[] args) {
	boolean ok = true;

	A a = new A();
	B b = new B();
	ok = ok && a.f() && ! b.f();

	a = b;
	ok = ok && ! a.f() && ! b.f();

	a = new B();
	ok = ok && ! a.f();

	if (ok)
	    System.out.print("ok\n");
    }
}
