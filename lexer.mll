(* Analyseur lexical pour MiniJava *)

{
  open Lexing
  open Parser

  (* Keywords *)
  let id_or_kwd =
    let h = Hashtbl.create 32 in
    List.iter (fun (s, tok) -> Hashtbl.add h s tok)
      [
      "boolean", BOOLEAN;
      "class", CLASS;
      "else", ELSE;
      "extends", EXTENDS;
      "false", FALSE;
      "if", IF;
      "implements", IMPLEMENTS;
      "int", INT;
      "interface", INTERFACE;
      "new", NEW;
      "null", NULL;
      "public", PUBLIC;
      "return", RETURN;
      "static", STATIC;
      "this", THIS;
      "true", TRUE;
      "void", VOID;
      "while", WHILE;
      ];
    fun s -> try Hashtbl.find h s with Not_found -> IDENT s

  let string_buffer = Buffer.create 1024
}

let digit = ['0'-'9']
let alpha = ['a'-'z' 'A'-'Z']
let ident = (alpha | '_') (alpha | digit | '_')*

rule token = parse
  | ("//" [^ '\n']*)? "\n"
    { new_line lexbuf; token lexbuf }
  | "/*"
    { comment lexbuf; token lexbuf }
  | '"'
    { string lexbuf }
  | "\t" | " "
    { token lexbuf }
  | ident as id
    { id_or_kwd id }
  | '0' | ['1'-'9'] digit* as s
    { try CINT (int_of_string s) with _ ->
      raise(Error.Lexing_error("constant too big: "^s)) }
  | '+' { PLUS } | '-' { MINUS }
  | '*' { TIMES } | '/' { DIV } | '%' { MOD }
  | "&&" { AND } | "||" { OR }
  | '.' { DOT } | ';' { SEMICOL } | ',' { COMMA } | '!' { NOT }
  | '(' { LPAREN } | ')' { RPAREN } | '{' { LACC } | '}' { RACC }
  | '[' { LBRA } | ']' { RBRA }
  | '=' { AFFECT }
  | "==" { EQUAL } | "!=" { NEQUAL }
  | "<" { LTHAN } | "<=" { LEQ } | ">" { GTHAN } | ">=" { GEQ }
  | '&' { ESP }
  | eof { EOF }
  | _ { raise(Error.Lexing_error "unexpected character") }

and comment = parse
  | "*/" { () }
  | '\n'  { new_line lexbuf; comment lexbuf }
  | _   { comment lexbuf }
  | eof { raise(Error.Lexing_error "EOF while scanning C-style comment") }

and string = parse
  | '"'
    { let s = Buffer.contents string_buffer in
    Buffer.reset string_buffer;
    CSTR s }
  | "\\n"
    { Buffer.add_char string_buffer '\n';
    string lexbuf }
  | "\\\""
    { Buffer.add_char string_buffer '"';
    string lexbuf }
  | "\\\\"
    { Buffer.add_char string_buffer '\\';
    string lexbuf }
  | '\\'
    { raise (Error.Lexing_error "Incorrect escape sequence.") }
  | '\n'
    { raise (Error.Lexing_error "Newline in string.") }
  | _ as c
    { Buffer.add_char string_buffer c;
    string lexbuf }
  | eof
    { raise (Error.Lexing_error "EOF while scanning string") }

{
  
}
