/*
  parcours infixe d'un arbre binaire, en espace constant
  et en temps linéaire

  Joseph M. Morris
  Traversing binary trees simply and cheaply
  Information Processing Letters 9(5), 1979
*/

class Bintree {
  int value;
  Bintree left;
  Bintree right;

  Bintree(Bintree left, int value, Bintree right) {
    this.left = left; this.value = value; this.right = right;
  }

  Bintree perfect(int h) {
    if (h == 0)
      return null;
    return new Bintree(perfect(h-1), h, perfect(h-1));
  }

  Bintree left(int h) {
    Bintree t = null;
    while (h > 0) {
      t = new Bintree(t, h, null);
      h = h - 1;
    }
    return t;
  }

  Bintree right(int h) {
    Bintree t = null;
    while (h > 0) {
      t = new Bintree(null, h, t);
      h = h - 1;
    }
    return t;
  }
}

class Morris {
  void inorder(Bintree t) {
    while (t != null) {
      if (t.left == null) {
        // pas de sous-arbre gauche : on visite le noeud et on descend à droite
        System.out.print(t.value+"."); // <-- visiter t
        t = t.right;
      } else {
        // un sous-arbre gauche : on cherche le prédécesseur p et t
        Bintree p = t.left;
        while (p.right != null && p.right != t) {
          p = p.right;
        }
        if (p.right == null) {
          // on a trouvé p . on le fait pointer sur t et on descend à gauche
          p.right = t;
          t = t.left;
        } else {
          // on est retombé sur t . on avait déjà visité le sous-arbre gauche
          // on restaure p.right, on visite t et on descend à droite
          p.right = null;
          System.out.print(t.value+"."); // <-- visiter t
          t = t.right;
        }
      }
    }
    System.out.print("\n");
  }
}

class Main {
  public static void main(String[] args) {
    Morris m = new Morris();
    Bintree t = null;
    m.inorder(t);
    t = new Bintree(null, 42, null);
    m.inorder(t);
    t = new Bintree(null, 41, t);
    m.inorder(t);
    t = new Bintree(t, 43, null);
    m.inorder(t);
    t = t.perfect(3);
    m.inorder(t);
    t = t.perfect(5);
    m.inorder(t);
    t = t.left(5);
    m.inorder(t);
    t = t.right(5);
    m.inorder(t);
  }
}
