
class A {}
class C {}
interface I<X extends A> {}
class E implements I<C> {}
class Main { public static void main(String[] args) { } }
