
interface Monoid<T> {
  T unit();
  T mul(T x);
}

// exponentiation rapide

class Expo<M extends Monoid<M>> {

  Expo() {}

  M exp(M x, int n) {
    if (n == 0) return x.unit();
    M r = exp(x.mul(x), n / 2);
    if (n % 2 == 0)
      return r;
    else
      return x.mul(r);
  }

}

// matrices 2x2

class Mat22 implements Monoid<Mat22> {

  int m00;
  int m01;
  int m10;
  int m11;

  Mat22(int m00, int m01, int m10, int m11) {
    this.m00 = m00;
    this.m01 = m01;
    this.m10 = m10;
    this.m11 = m11;
  }

  public Mat22 unit() {
    return new Mat22(1, 0, 0, 1);
  }

  public Mat22 mul(Mat22 x) {
    return new Mat22(this.m00 * x.m00 + this.m01 * x.m10,
                     this.m00 * x.m01 + this.m01 * x.m11,
                     this.m10 * x.m00 + this.m11 * x.m10,
                     this.m10 * x.m01 + this.m11 * x.m11);
  }

}

class FastFib {

  FastFib() {}

  int fib(int n) {
    Mat22 mfib = new Mat22(1, 1, 1, 0);
    Expo<Mat22> e = new Expo<Mat22>();
    Mat22 r = e.exp(mfib, n);
    return r.m01;
  }

}

class Main {

  public static void main(String[] args) {
    FastFib ff = new FastFib();
    int n = 0;
    while (n < 20) {
      System.out.print("fib("+n+")=" + ff.fib(n) + "\n");
      n = n + 1;
    }
  }

}
