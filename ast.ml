(* Syntaxe abstraite pour MiniJava *)

open Lexing

type local = Lexing.position * Lexing.position

and ident = {str: string; loc: local}

type ntyp = { name: ident; params: ntyp list}

type typ = TVoid | TNull | TBool | TInt | TNtyp of ntyp

type param = {t: typ; name: ident}
type paramtyp = ntyp

type op =
	| OEq | ONeq | OLt | OLeq | OGt | OGeq
	| OPlus | OMinus | OMul | ODiv | OMod | OAnd | OOr

type const = CInt of int | CString of string | CBool of bool

type access = AId of ident | AMem of sexpr * ident
and expr_desc =
	| ENull | ESimple of sexpr | EAffect of access * expr
	| ENot of expr | EUminus of expr
	| EOp of op * expr * expr
and sexpr_desc =
	| SConst of const | SThis | SExpr of expr
	| SNew of ntyp*(expr list)
	| SEval of access*(expr list)
	| SAccess of access
and expr = {expr: expr_desc; loc: local}
and sexpr = {sexpr: sexpr_desc; loc: local}

type instr =
	| INop | ISexpr of sexpr | IAffect of access * expr
	| IVdecl of param | IVdeclinit of param * expr
	| IIf of expr * instr | IIfElse of expr * instr * instr
	| IWhile of expr * instr | IBlock of instr list
	| IReturn of (expr option) * local

type constr = {name: ident; params: param list; instrs: instr list}
type proto = { public: bool; t: typ; name: ident; params: param list}
type meth = {proto: proto; instrs: instr list}

type decl = DArg of param | DConstr of constr | DMeth of meth

type clas = { name: ident; paramtyps: paramtyp list; extends: ntyp option;
                    implements: ntyp list; funs: decl list}

type intf = { name: ident; paramtyps: paramtyp list;
                extends: ntyp list; protos: proto list}

type clas_intf = Clas of clas | Intf of intf

type clas_main = { param: ident; instrs: instr list}

type program = { classes: clas_intf list; main: clas_main  }

